/////////////////////////////////////////////
// the main application configuration file //
/////////////////////////////////////////////

module.exports = {

  //////////
  // MAIL //
  //////////

  mail: {
    // who emails appear to be sent from
    sender: {
      name: 'TPG Media',
      address: 'tom@websir.co.uk'
    },

    // delivery address of contact queries
    contact: {
      name: 'TPG Media',
      address: 'info@tpg-media.com'
    },

    contactCC: {
      addresses: 'keithlangford@hotmail.com,tom@websir.co.uk,k.heffey@gmail.com'
    },

    //newsletter subscribers notifications
    newsletter: {
      name:    'TPG Media',
      address: 'info@tpg-media.com',
      cc: 'keithlangford@hotmail.com,tom@websir.co.uk,k.heffey@gmail.com',
//      address: 'tom@websir.co.uk',
      subject: 'New newsletter subscriber'
    },

    //subscription email receiver
    subscriber: {
      address: 'keith@tpg-media.com'
    }

  },

  /////////////
  //  PAGES  //
  /////////////

  pages: {
    testimonials: {
      name: 'Testimonial'
    },
    contactus: {
      name: 'Contact Us'
    }
  },

  /////////////
  // DOMAINS //
  /////////////

  domains: {

    'emirates': {
      magazine: 'emirates',
      name: 'Emirates',
      bottomw: '100%;',
      status: 1
    },

    'bahrain': {
      magazine: 'bahrain',
      name: 'Bahrain',
      bottomw: '100%;',
      status: 1
    },

    'kuwait': {
      magazine: 'kuwait',
      name: 'Kuwait',
      bottomw: '100%;',
      status: 1
    },

    'qatar': {
      magazine: 'qatar',
      name: 'Qatar',
      bottomw: '100%;',
      status: 1
    },

    'saudi': {
      magazine: 'saudi',
      name: 'Saudi',
      bottomw: '100%;',
      status: 1
    },

    'oman': {
      magazine: 'oman',
      name: 'Oman',
      bottomw: '100%;',
      status: 1
    }
  },

  ////////////////
  // CATEGORIES //
  ////////////////

  categories: [
    'local',
    'regional',
    'worldwide',
    'we-chat-with',
    'sustainability',
    'infrastructure',
    'rail-and-transport',
    'ceramics',
    'oil-and-gas'
  ],

  //////////////
  // SERVICES //
  //////////////

  aws_mailer: {
    accessKey: "AKIAJHQ2INDEQLV6PCCA",
    secretKey: "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",
    serviceUrl: "https://email.eu-west-1.amazonaws.com",
    region:    "eu-west-1",

    bucket: 'tpgmedia-test'
  },

  aws: {
    accessKey: "AKIAJR6UAZIUKV2OVYQQ",
    secretKey: "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",
    region:    "us-west-2",

    bucket: 'tpgmedia-test'
  },

  paymill: {
    test: {
      private: "9443755258e6ee388cffa72ac478837b",
      public:  "352783883990167a437afceb5e5a042c"
    },

    live: {
      private: "xxxxxxxx",
      public:  "xxxxxxxx"
    }
  }
};
