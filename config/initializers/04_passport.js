var passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy
  , User = require('../../app/models/user');

module.exports = function() {

  // registry local strategy
  passport.use(new LocalStrategy({
      usernameField: 'email',
      passwordField: 'password'
    },

    function(email, password, done) {
      User.findOne({ email: email }, function(err, user) {
        // was there an error?
        if (err)
          return done(err);

        // does the user exist?
        if (!user)
          return done(null, false, { message: 'No such user'});

        // check password
        user.isPassword(password, function(err, valid) {
          if (!valid || err) {
            return done(null, false, { message: 'Incorrect password' });
          } else {
            return done(null, user);
          }
        });
      });
    }
  ));

  // serialise for session
  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });

  // deserialise for session
  passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user);
    });
  });
};
