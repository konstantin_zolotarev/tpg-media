// bread and butter
var express = require('express');

// useful utilities
var util = require('util');

// authentication
var passport = require('passport');

// jade
var jade = require('jade');

// stylus + nib, javascript
var connectAssets = require('connect-assets');

// database
var mongoose = require('mongoose');
var mongoStore = require('connect-mongodb');


module.exports = function() {
  // cache this
  var self = this;

  // Warn of version mismatch between global "lcm" binary and local installation
  // of Locomotive.
  if (this.version !== require('locomotive').version) {
    console.warn(util.format('version mismatch between local (%s) and global (%s) Locomotive module', require('locomotive').version, this.version));
  }

  // Configure application settings.  Consult the Express API Reference for a
  // list of the available [settings](http://expressjs.com/api.html#app-settings).
  this.set('views', __dirname + '/../../app/views');
  this.set('view engine', 'jade');

  // Register jade as a template engine.
  this.engine('jade', jade.__express);
  this.format('html', { extension: '.jade' })

  // Register the Mongoose adapter for our datastore
  this.datastore(require('locomotive-mongoose'));

  // Register formats for content negotiation.  Using content negotiation,
  // different formats can be served as needed by different clients.  For
  // example, a browser is sent an HTML response, while an API client is sent a
  // JSON or XML response.
  /* this.format('xml', { engine: 'xmlb' }); */

  // Use middleware.  Standard [Connect](http://www.senchalabs.org/connect/)
  // middleware is built-in, with additional [third-party](https://github.com/senchalabs/connect/wiki)
  // middleware available as separate modules.
  // middleware below

  // Accept put and delete verbs.
  this.use(express.bodyParser());
  this.use(express.methodOverride());

  // we need cookies and sessions
  this.use(express.cookieParser());
  this.use(express.cookieSession({
    secret: 'qawd1234ads78ui65t6y54rfe2asdasdgh',
    cookie: {
      // max age in milliseconds
      // this cookie will last a month
      maxAge: 1000 * 60 * 60 * 24 * 7 * 4
    }
  }));

  // passport!
  this.use(function (req, res, next) {
    if (req.method == 'POST' && req.url == '/login') {
      if (!req.body.rememberme) {
        req.session.cookie.maxAge = 1000 * 60 * 3;
      } else {
        req.session.cookie.expires = false;
      }
    }
    next();
  });
  this.use(passport.initialize());
  this.use(passport.session());

  // add validator
  this.use(require('express-validator')());

  // we need connect-flash to keep data between redirects
  this.use(require('connect-flash')());
  this.use(function(req, res, next) {
    if( req.flash('notices').length ) { res.locals.notices = req.flash('notices'); }
    if( req.flash('errors') ) { res.locals.errors = req.flash('errors'); }

    next();
  });

  // domain routing
  this.use(function(req, res, next) {
    // get the host we're using
    var domain = req.host;

    // save session if we try to force
    if (req.param('force'))
      req.session.force = req.param('force');

    // ruin the session if we don't want to force
    if (req.param('reset'))
      req.session.force = undefined;

    // set the domain if we're forcing
    if (req.session.force)
      domain = req.session.force;

    // default to oman
    if (!self.get('config').domains.hasOwnProperty(domain))
      domain = 'emirates';

    // set the current magazine identity
    self.set('magazine', self.get('config').domains[domain].magazine);
    self.set('name',     self.get('config').domains[domain].name);
    self.set('domains',  self.get('config').domains);

    // done
    next();
  });

  // global variables
  this.use(function(req, res, next) {
    self.set('user', req.user);

    next();
  });

  // domain (not URL) middleware to protect from crashes
  this.use(require('express-domain-middleware'));

  // our router
  this.use(this.router);

  // assets
  this.use(connectAssets({
    src: __dirname + '/../../app/assets',
    buildDir: 'public'
  }));
}
