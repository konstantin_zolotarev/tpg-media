var express = require('express');

module.exports = function() {
  var mongoUri = process.env.MONGOLAB_URI ||
    process.env.MONGOHQ_URL ||
    'mongodb://localhost/test';

  this.set('mongodb uri', mongoUri);

  // Log all queries
  require('mongoose').set('debug', function(collection, method, query, options) {
    // logger?
    // logger.debug('mongoose - ' + collection + '.' + method + '(' + JSON.stringify(query) + ') - ' + JSON.stringify(options));
  });

  // logging, for debug purposes
  this.use(express.logger('tiny'));

  // stack traces on errors
  this.use(express.errorHandler());

  // serve development static content
  this.use(express.static(__dirname + '/../../app/assets'));
}
