var express = require('express');

module.exports = function() {
  var mongoUri = process.env.MONGOLAB_URI ||
    process.env.MONGOHQ_URL ||
    'mongodb://localhost/test';
  // webfaction mongo stuff
  this.set('mongodb uri', mongoUri);

  // serve production static content
  this.use(express.static(__dirname + '/../../public'));
//  this.use(express.static(__dirname + '/../../app/assets'));

}
