module.exports = function routes() {
  // root
  this.root('pages#index');


  // static pages
  this.match('/contact', 'pages#contact');
  this.match('/testimonials', 'pages#testimonials');
  this.match('/free-features', 'pages#freeFeatures');
  this.match('/advertise', 'pages#advertise');
  this.match('/download/:id', 'pages#downloadissue');
  this.match('/weather', 'pages#weather');

  this.get('/subscribe', 'pages#subscribe');
  this.post('/subscribe', 'pages#new_subscription');


  // post data here to send contact mails
  this.post('/message', 'pages#message');

  // news routes
  this.match( '/news/:yyyy/:mm/:dd/:title', 'news#show' );
  this.match( '/news/:category', 'news#by_category' );

  this.resources('news');
  this.resources('events');
  this.resources('jobs');
  this.resources('issues');
  this.resources('interviews');
  this.resources('mediapack');

  this.resources('adverts', { only: ['index', 'show'] } );


  // feed stuff
  this.match( '/feed', 'feed#index' );
  this.match( '/feed/:category', 'feed#index' );


  // user account management
  this.resource('account');
  this.match('/account/login', 'account#login');
  this.post('/account/login', 'account#doLogin');
  this.match('/account/logout', 'account#logout');
  this.match('/account/lost', 'account#lost');
  this.post('/account/searching', 'account#searching');
  this.match('/account/found/:token', 'account#found');
  this.post('/account/reset', 'account#reset');
  this.get('/account/deletecv/:userid', 'account#deletecv', { via: [ 'GET' ] });
  this.post('/account/newsletter', 'account#newsletter');


  // debug fun
  this.match('/account/mail', 'account#mail');


  // search
  this.match('/search', 'search#index');


  // test
  this.post( '/upload', 'pages#upload');
  this.post( '/upload/:model/:id', 'pages#upload');
  this.get( '/capture/adverts/:id', 'adverts#impression');


  // admin area
  this.namespace('admin', function() {
    this.root('admin#index');

    this.resources('news');
    this.resources('users');
    this.resources('subscriptions');

    this.resources('videos');
    this.resources('issues');
    this.resources('events');
    this.resources('jobs');
    this.resources('issues');

    this.resources('pages');
    this.resources('testimonials');
    this.resources('interview');
    this.resources('mediapack');

    this.resources('adverts');
    this.match('/adverts/:id/:type/csv/:from/:to', 'adverts#csv');
    this.match('/news/help', 'news#help');
  });
}

