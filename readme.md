# TPG Media

Node project for TPG media magazine websites.

## Install and run

    $ sudo npm install -g locomotive
    $ sudo npm install -g less
    $ npm install
    $ node server.js

## Development

Start a development server with

    $ node app/seed.js
    $ NODE_ENV=development node server.js

#### Using node-inspector

First install and run node-inspector

    $ sudo npm install -g node-inspector
    $ node-inspector

Start a development server with the debug flag

    $ NODE_ENV=development lcm server --debug

Then open [http://127.0.0.1:8080/debug?port=5858](http://127.0.0.1:8080/debug?port=5858) in your browser. Note, debugging
remotely is possible.

#### Using a different port

Easy!

    $ NODE_ENV=development lcm server -p 4000

## Production

Start a production server with

    $ NODE_ENV=production lcm server

Javascript and CSS will automatically be crunched into single files.

## Database setup

You'll need mongodb running on your local machine. Install mongodb and start with

    $ mongod

# Stack references

  * nginx as http proxy server for each vhost
  * [node.js](http://nodejs.org/) powered
  * [locomotive](http://locomotivejs.org/) framework
  * [express](http://expressjs.com/) framework
  * [mongodb](http://www.mongodb.org/) database
  * [mongoose](http://mongoosejs.com/) orm
  * [mocha](http://visionmedia.github.io/mocha/) for testing
  * [jade](http://jade-lang.com/) template engine
  * [stylus](http://learnboost.github.io/stylus/) stylesheet preprocessor
  * [nib](http://visionmedia.github.io/nib/) stylesheet mixins

