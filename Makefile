TESTS = test/*

test:
	mocha --timeout 5000 --reporter nyan --recursive $(TESTS)

test-w:
	mocha --timeout 5000 --reporter spec --growl --watch --recursive $(TESTS)

.PHONY: test test-w