var mongoose = require("mongoose");

var newsModel = require( "../../app/models/news" );
var News = mongoose.model('News');

describe('News: ', function() {
  var news;

  // Create and save a news to use for tests.
  before(function(done){
    News.create({
      title:"Test title",
      content:"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
    }, function(err,n) {
      news = n;
      done();
    });
  });

  after(function(done) {
    news.remove();
    done();
  });


  //
  describe('Creating a new Article', function(){
    it( 'should assign correctly', function() {
      news.should.not.equal(undefined);
    });

    it( 'should persist', function() {
      News.find({
        title: "Test title"
      }, function(err, doc) {
        doc.length.should.not.equal(0);
      })
    });

    describe("allows restriction options", function() {
      it("default to false", function() {
        news.restricted.should.equal(false);
      });

      it("stores change", function() {
        news.restrict(true, function(suc, n) {
          n.restricted.should.equal(true);
        });
        news.restrict(false, function(suc, n) {
          n.restricted.should.equal(false);
        });
      });
    });
  });

  //
  describe('Reading Articles', function(){
    describe('by date', function() {
      it('should correctly pull from database', function() {
        News.find({
          title: "Test title"
        }, function(err, doc) {
          doc[0].content.should.equal('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.');
        })

      });
    });
    describe('by id', function() {
      it('should correctly pull from database', function() {
        var id = news.id
      });
    });
  });

  //
  describe('Updating a news Article', function(){

  });

  //
  describe('Deleting an Article', function(){

  });

});