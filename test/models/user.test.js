var mongoose = require("mongoose");

var userModel = require( "../../app/models/user" );
var User = mongoose.model('User');


describe('Users: ', function() {
  var user;

  // Create and save a user to use for tests.
  before(function(done){
    User.create({
      name: "John smith",
      email: 'test123@example.com',
      phone:"0123456789",
      password: "password"
    }, function(err,u) {
      user = u;
      done();
    });
  });

  after(function(done) {
    user.remove();
    done();
  });


  //
  describe('Creating a new User', function(){
    it('should have an email', function(){
      user.email.should.equal('test123@example.com');
    });

    it('add to database', function() {
      User.find(user).count(function(err, count) { count.should.equal(1); });
    });

    it('enforces email uniqueness', function() {
      User.create({ name: "Nobody", email:"test123@example.com" }, function(err, u) {
        err.code.should.equal(11000);
      });
    });

    describe('validates telephone', function() {
      it('enforces minimum length', function() {
        User.create({ name: "Nobody", email:"test1234@example.com", phone:"0123" }, function(err, u) {
          err.errors.phone.name.should.equal('ValidatorError');
        });
      });

      it('enforces minimum length', function() {
        User.create({ name: "Nobody", email:"test12345@example.com", phone:"01234567890123" }, function(err, u) {
          err.errors.phone.name.should.equal('ValidatorError');
        });
      });
    });

    describe('password', function() {
      it('should store as password', function() { user.should.have.property('password'); });
      it('should encrypt using bcrypt', function() { user.password.should.not.equal('password'); });
    });

    describe('where passwords', function() {
      it('match', function() {
        user.isPassword('password', function(err, bool) { bool.should.equal(true); });
      });

      it('do not match', function() {
        user.isPassword('notpassword', function(e, bool) { bool.should.equal(false); });

      });
    });
  });

  //
  describe("Authorisation", function() {
    describe("adds auth level", function() {
      it("should be zero", function() {
        user.authorisation.should.equal(0);
        user.hasAuth(0).should.equal(true);
      });
    });

    it("disallows user", function() {
      mongoose.model('News').create({}, function(err, news) {
        user.canAccess(news).should.equal(true);
      });
    });

    describe("with toggle", function() {
      it("should be changable.", function() {
        mongoose.model('News').create({}, function(err, news) {
          news.restrict(true, function(suc,item) {
            user.canAccess( item ).should.equal(false);
          });
          news.restrict(false, function(suc,item) {
            user.canAccess( item ).should.equal(true);
          })

        });
      });
    });
  });


  //
  describe('Updating a User', function() {
    describe('should change info', function() {
      it( 'finds new user', function() {
        user.update( {
          name: "Jane Doe"
        }, function(err, n, resp) {
          User.find({ name: "Jane Doe" }, function(err,doc) {
            doc.length.should.equal(1);
          });
        });
      });

      it( 'does not find old user', function() {
        user.update( {
          name: "Jane Doe"
        }, function(err, n, resp) {
          User.find({ name: "John Smith" }, function(err,doc) {
            doc.length.should.equal(0);
          });
        });
      });
    });

    describe("should allow auth change", function() {
      before( function() {
        user.setAuth('admin');
      });

      it("store correctly", function() {
        user.authorisation.should.equal(4);
      });

      describe("for queries", function() {
        it("by name", function() {
          user.hasAuth('admin').should.equal(true);
        });
        it("by number", function() {
          user.hasAuth(4).should.equal(true);
        });
      });
    });
  });

  //
  // Not sure how to test this.
  describe('Deleting a User', function() {

  });
});