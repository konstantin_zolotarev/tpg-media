var locomotive = require('locomotive')
  , Controller = locomotive.Controller;

var News = require('../models/news')
  , RSS = require('rss');

var FeedController = new Controller();

// GET /feed
// Return an RSS feed (XML)
FeedController.index = function() {
  var self = this;

  var filter = {
    magazines: { $all: [ this.app.get('magazine') ] },
    published: true
  };

  // category
  this.category = this.param('category');
  if (this.category != undefined)
    filter.category = this.category;

  // get recent news posts
  News.find(filter).sort({
    'published_at': 'desc'
  }).limit(20).execFind(function(err, news) {
    var feed = new RSS({
      title: 'TPG Media - ' + self.app.get('name'),
      description: 'TPG Media, ' + self.app.get('name') + ' magazine',
      feed_url: 'http://' + self.req.host + '/feed',
      site_url: 'http://' + self.req.host,
      image_url: 'http://' + self.req.host + '/rss',

      author: 'TPG Media',
      copyright: 'TPG Media',
      language: 'en',
      pubDate: Date.now(),
      ttl: '60'
    });

    // for each news item
    for(var index in news) {
      var article = news[index];

      // add item
      feed.item({
        title: article.title,
        description: article.content,
        url: article.pretty_url(),
        date: article.published_at
      });
    }

    // set content-type so browser can display nicely
    self.res.set('Content-Type', 'text/xml');

    // send!
    self.res.send(feed.xml());
  });
};

FeedController.before(['index'], require('../helpers/firewall').subscribed);

module.exports = FeedController;
