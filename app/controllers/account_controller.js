var locomotive = require('locomotive')
  , Controller = locomotive.Controller
  , imgHandler = require('../helpers/imageHandler')
  , crypto = require('crypto');

// User object
var User = require('../models/user');

// passport
var passport = require('passport');

// new controller
var AccountController = new Controller();

// GET /account/new
// Account signup page
AccountController.new = function() {
  // render template, try to recover variables from flash
  this.render({
    errs: this.req.flash('error-signup'),

    name: this.req.flash('name'),
    email: this.req.flash('email')
  });
}

// POST /account
// Process account signup
AccountController.create = function() {
  var self = this;

  // validate input
  this.req.assert('name', 'A valid name is required').notEmpty().len(3,30);
  this.req.assert('email', 'A valid email address is required').isEmail();
  this.req.assert('password', 'The password is too short').len(8);
  this.req.assert('password', 'The passwords do not match').is(this.param('password_confirmation'));

  // get an array of any errors
  var errors = this.req.validationErrors();

  // we could probably flash the name/email too!
  this.req.flash('name',  this.param('name'));
  this.req.flash('email', this.param('email'));

  // if there is a validation error
  if (errors) {
    // flash error data to session
    this.req.flash('error-signup', errors);

    // redirect to signup page
    return this.redirect(self.urlFor({ action: 'new' }));
  }

  // got this far, so register the user
  var user = new User({
    name: self.param('name'),
    email: self.param('email'),
    password: self.param('password')
  });

  user.save(function(err) {
    if (err) {
      self.req.flash('error-signup', [{
        msg: 'Email address already in use.'
      }]);

      // redirect to signup page
      return self.redirect(self.urlFor({ action: 'new' }));
    } else {
      var mailer = require('../helpers/mailer');
      mailer.userMail('Account created', 'account/new', user);

      self.render();
    }
  });
};

// GET /account/login
// Show login page
AccountController.login = function() {
  this.render({
    error: this.req.flash('error'),
    message: this.req.flash('message'),
    email: this.req.flash('email'),

    path: this.param('path') || ''
  });
}

// POST /account/login
// Process account login
AccountController.doLogin = function() {
  var self = this;

  var success = self.urlFor({ action: 'show' });

  // are we redirecting after successful login?
  if (this.param('path') != '')
    success = this.param('path');

  // attempt login
  passport.authenticate('local', {
    successRedirect: success || '/',
    failureRedirect: '/',//self.urlFor({ action: 'login' }),
    failureFlash: true
  })(this.__req, this.__res, this.__next);
}

// GET /account/logout
// Logout
AccountController.logout = function() {
  this.req.logout();
  this.redirect('/');
}

// GET /account/lost
// Start password reset process
AccountController.lost = function() {
  if (this.req.session.error) {
    this.error = true;
    this.req.session.error = undefined;
  }

  this.render();
}

// POST /account/searching
// Send request to get email reset token
AccountController.searching = function() {
  var self = this;

  // save the email address
  this.email = this.param('email');

  // try to get the user
  User.findOne({ email: this.email }, function(err, user) {
    if (err)
      throw err;

    if (user == undefined || !user) {
      self.req.session.error = true;
      self.redirect('/account/lost');
    } else {
      // generate token
      self.token = crypto.createHash('sha256').
        update(new Date().toString() + user.password).digest('hex');

      // save token
      user.token = self.token;

      // save the user
      user.save(function(err) {
        if (err) {
          throw err;
        } else {
          // email the token
          var mailer = require('../helpers/mailer');
          mailer.userMail('Password reset request', 'account/reset_request', user, self.req.host);

          // render page
          self.render();
        }
      });
    }
  });
}

// GET /account/found/:token
// Password reset form
AccountController.found = function() {
  // token
  this.token = this.param('token');

  if (this.req.session.error)
    this.error = true;

  // render
  this.render();
}

// POST /account/update
// Send password reset request
AccountController.reset = function() {
  var self = this;

  // get token
  this.token = this.param('token');

  // check we have a token
  if (this.token == undefined)
    throw 'no token?';

  // new passwords
  var newPassword = this.param('new_password');
  var newPasswordConfirmation = this.param('new_password_confirmation');

  if (newPasswordConfirmation != newPassword) {

    this.req.session.error = true;
    this.redirect('/account/found/' + this.token);

  } else {

    // find user
    User.findOne({ token: this.token }, function(err, user) {
      if (err || user == null) {

        // bounce to start of process
        self.req.session.error = undefined;
        self.redirect('/account/lost');

      } else {
        // save new password
        user.password = newPassword;

        // expire token by generating a new one
        user.token = crypto.createHash('sha256').
          update(new Date().toString() + user.password).digest('hex');

        user.save(function(err) {
          if (err)
            throw err;

          // send email!
          var mailer = require('../helpers/mailer');
          mailer.userMail('Password reset successful', 'account/reset_success', user, self.req.host);

          self.render();
        });
      }
    });
  }
}

// POST /account/newsletter
// send subscribe newsletter email
AccountController.newsletter = function() {
  console.log(this.param('email'));

  if (this.param('email')) {
    var mailer = require('../helpers/mailer');
    mailer.newsLetter('newsletter', this.param('email'));

    this.res.send(200);
  }

  return false;
}

// GET /account/mail
// A test function for sending mail
AccountController.mail = function() {
  var mailer = require('../helpers/mailer');
  mailer.test('Test email', 'test', this.req.user);

  this.res.send(200);
}

// GET /account
// Show account page
AccountController.show = function() {
  this.user = this.req.user;

  // updated variable
  this.updated = this.req.session.updated;
  this.req.session.updated = undefined;

  this.render();
}

// GET /account/edit
// Edit account page
AccountController.edit = function() {
  this.user = this.req.user;

  // error session messages
  this.error = this.req.session.error;
  this.message = this.req.session.message;
  this.req.session.error = undefined;
  this.req.session.message = undefined;

  // do we have some form data?
  if (this.req.session.form == undefined) {
    this.form = {
      'name':  this.user.name,
      'email': this.user.email,
      'phone': this.user.phone
    }
  } else {
    this.form = this.req.session.form;
    this.req.session.form = undefined;
  }

  // finally convert validation error to useful data
  if (this.req.session.validation != undefined) {
    // we have validation errors
    var validation = this.req.session.validation;

    if (validation.errors.hasOwnProperty('name'))
      this.nameError = true;

    if (validation.errors.hasOwnProperty('phone'))
      this.phoneError = true;

    if (validation.errors.hasOwnProperty('email'))
      this.emailError = true;

    this.req.session.validation = undefined;
  }

  this.render();
}

// PUT /account
// Process account edit
AccountController.update = function() {
  var self = this;
  var _file = this.req.client.parser.incoming.files.cv;
  var _userId = this.req.user.id;

  if ( _file && _file.originalFilename !== ''){
    imgHandler.targetDirectory = 'user/'+ _userId +'/';

    imgHandler.uploadFile(_file.path,
      imgHandler.targetDirectory + _file.name,
      function(res){
        User.findOneAndUpdate({
            _id: _userId
          }, {
            cv: ( res.req.url ? res.req.url : '' ),
            modified_at: Date.now()
          }, function(err, doc){
            if (err){
              throw err;
            }
          }
        );
      }
    );
  }

  // find user
  User.findOne({
    _id: _userId
  }, function(err, user) {
    if (err) {
      self.redirect('/account');
    } else {
      var validateAndSave = function() {
        // check validation
        user.validate(function(err) {
          if (err) {
            console.log(require('util').inspect(err));

            self.req.session.error = true;
            self.req.session.validation = err;
            self.req.session.message = 'Validation error.';

            self.redirect('/account/edit');
          } else {
            // save the user
            user.save(function(err) {
              if (err) {
                self.req.session.error = true;
                self.req.session.message = 'An unknown error occured.';

                self.redirect('/account/edit');
              } else {
                self.req.session.updated = true;
                self.redirect('/account');
              }
            });
          }
        });
      };

      // set values
      user.name  = self.param('name');
      user.email = self.param('email');
      user.phone = self.param('phone');

      // save form values
      self.req.session.form = {
        'name': user.name,
        'email': user.email,
        'phone': user.phone
      };

      // password values
      var current = self.param('password');
      var newpass = self.param('newpassword');
      var curpass = self.param('newpassword_confirm');

      // do we need a valid password?
      if (user.isModified('email') || self.param('newpassword') != '') {
        // check current password is correct
        user.isPassword(current, function(err, correct) {
          if (correct) {
            // continue
            if (newpass != '') {
              if (newpass == curpass) {
                // set the new password
                user.markModified('password');
                user.password = newpass;

                // go go go
                validateAndSave();
              } else {
                // new passwords do not match
                self.req.session.error = true;
                self.req.session.message = 'New passwords do not match.';

                self.redirect('/account/edit');
              }
            } else {
              validateAndSave();
            }
          } else {
            // incorrect password
            self.req.session.error = true;
            self.req.session.message = 'Current password was incorrect.';

            self.redirect('/account/edit');
          }
        });
      } else {
        // no password check requirement
        validateAndSave();
      }
    }
  });
}

// DELETE /account
// Process account deletion
AccountController.destroy = function() {
  this.redirect('/');
}

//GET /account/deletecv/:userid
// Process cv file deletion
AccountController.deletecv = function() {
  var self = this;
  var _userId = this.param('userid');

  User.findOneAndUpdate({
      _id: _userId
    }, {
      cv: '',
      modified_at: Date.now()
    }, function(err, doc){
      if (err){
        throw err;
      }

      return self.res.send(200);
    }
  );
}

// Protect methods that require authentication with firewall
AccountController.before(['destroy', 'edit', 'logout', 'show', 'update'], require('../helpers/firewall').none);

module.exports = AccountController;
