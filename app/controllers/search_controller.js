var locomotive = require('locomotive')
, Controller = locomotive.Controller;

var News = require('../models/news');

var SearchController = new Controller();

// GET /search
// Shows search results (uses GET so results can be copied/pasted)
SearchController.index = function() {
  var self = this;

  // save the queries
  this.query    = this.param('query');
  this.tag      = this.param('tag');
  this.category = this.param('category');

  var searchOptions = {
    // project: '-user_id -published_at -modified_at',
    filter: {
      magazines: {
        $all: [ this.app.get('magazine') ]
      },
      published: true
    },
    limit: 50
  };

  if (this.tag !== undefined && this.tag != '')
    searchOptions.filter.tags = { $all: [this.tag] };

  if (this.category !== undefined && this.category != '')
    searchOptions.filter.category = this.category;

  // query?
  if (this.query !== undefined && this.query != '') {
    // do a search
    var regExp = new RegExp(self.query, 'i');
    News.find({
      '$or': [
        {title: regExp},
        {content: regExp}
      ],
      magazines: {
        $all: [ this.app.get('magazine') ]
      },
      published: true
    }, null, {limit: 50}, function(err, docs) {
      if (err)
        throw err;

      return self.render({
        results: docs.map(function(element) {
          return {
            obj: element,
            score: 1
          };
        })
      });
    });
  } else {
    // fake a blank search
    this.query = '';

    // get 50 most recent
    News.find(searchOptions.filter).sort({'published_at': 'desc'}).limit(50).exec(function(err, results) {
      if (err)
        throw err;


      self.render({
        // map it to the same format that textSearch returns
        results: results.map(function(element) {
          return {
            obj: element,
            score: 1
          };
        })
      });
    });

    // render with empty result set
  }
}

module.exports = SearchController;
