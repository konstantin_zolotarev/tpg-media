var locomotive = require('locomotive')
  , Controller = locomotive.Controller;

var Issue = require('../models/issue');
var IssuesController = new Controller();

// GET /Issues
// List Issues
IssuesController.index = function () {
  var self = this;
  Issue.find({
//    magazines: { $all: this.req.user.getSubscriptions() }
    magazines: { $all: [ this.app.get('magazine') ] }
  }, null, {sort: {modified_at: -1}},  function (e, list) {
    if (e) throw e;
    self.render({
      issues: list
    });
  });
};

// GET /Issues/:id
// Show Issue details
IssuesController.show = function () {
  var self = this;
  Issue.findById(self.req.params.id, function (e, doc) {
    if (e) throw e;
    self.render({
      issue: doc
    });
  });
};

//IssuesController.before('*', require('../helpers/firewall').subscribed);

module.exports = IssuesController;
