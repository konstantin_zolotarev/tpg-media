var locomotive = require('locomotive')
  , Controller = locomotive.Controller
  , imgHandler = require('../../helpers/imageHandler');

var MediaPack = require('../../models/mediapack');

// new controller
var MediaPackController = new Controller();


// GET /admin/testimonials
// Show list of all testimonials
MediaPackController.index = function() {
  var self = this;

  // get pagination variables with defaults
  this.page   = parseInt(this.param('page'), 10) || 1;
  this.limit  = parseInt(this.param('limit'), 10) || 10;

  // make sure these values are safe!
  if (this.page  < 1) this.page  = 1;
  if (this.limit < 1) this.limit = 1;

  var filter =  {};

  MediaPack.paginate({
    query: filter,

    sort: (this.order_by == 'desc' ? '-' : '') + this.sort_by,

    page:  this.page,
    limit: this.limit
  }, function(err, provider){
    if (err)
      throw err;

    self.render({
      docs: provider.docs,
      page: provider.page,
      limit: self.limit,
      pages: provider.pages,
      total: provider.count,
      length: provider.docs.length
    });
  });


//  MediaPack.find({}, function(e, docs){
//    if (e) throw e;
//
//    // Should we remove these from the db here?
////    docs = docs.filter( function(x) { return (x.image && x.image.length > 0) } );
//
//    self.render({
//      docs: docs,
//      errors: self.req.flash('error')
//    });
//  });
};

/**
 * Create new testimonials object
 */
MediaPackController.new = function() {
  var self = this;

  self.render({
    doc: {},
    errors: self.req.flash('error')
  });
};

// GET /admin/pages/:id/edit
// Edit an Pages
MediaPackController.edit = function() {
  var self = this;

  MediaPack.findOne({
    _id: self.param('id')
  }, function(err, doc) {
    self.render({
      doc: doc,
      errors: self.req.flash('error')
    });
  });
};

// POST /admin/pages/:id
// Edit an Page
MediaPackController.update = function() {
  var self = this;
  var _file = this.req.client.parser.incoming.files.filesToUpload;
  var _params = this.req.params;

  console.log(_file);

  if (_file && _file.originalFilename !== '') {
    if (_file.headers['content-type'].indexOf('image') !== 0) {
      self.req.flash('error', 'Wrong image file !');
      self.redirect('/admin/mediapack/'+self.param('id')+'/edit');
      return;
    }

    imgHandler.targetDirectory = "mediapacks/"+ _params.id +"/";

    imgHandler.uploadFile( _file.path,
      imgHandler.targetDirectory + _file.name,
      function(res) {

        MediaPack.findOneAndUpdate({
          _id: self.param('id')
        }, {
          title: self.param('title'),
          link: self.param('link'),
          description: self.param('description'),
          image: ( res.req.url ? res.req.url : '' ),
          modified_at: Date.now()
        }, function(err, doc) {
          if (err)
            throw(err);

          self.redirect('admin/mediapack/');
        });
      });
  } else {
    console.log(self.param('id'));
    MediaPack.findOneAndUpdate({
      _id: self.param('id')
    }, {
      title: self.param('title'),
      link: self.param('link'),
      description: self.param('description'),
      modified_at: Date.now()
    }, function(err, doc) {
      if (err)
        throw(err);

      self.redirect('admin/mediapack/');
    });
  }
};

/**
 * Create new event
 */
MediaPackController.create = function() {
  var self = this;
  var _file = this.req.client.parser.incoming.files.filesToUpload;
  var _params = this.req.params;

  if (_file.headers['content-type'].indexOf('image') !== 0) {
    self.req.flash('error', 'Wrong image file !');
    self.redirect('/admin/mediapack/new');
    return;
  }

  imgHandler.targetDirectory = "mediapacks/"+ _params.id +"/";

  imgHandler.uploadFile( _file.path,
    imgHandler.targetDirectory + _file.name,
    function(res) {

      MediaPack.create({
        title: self.param('title'),
        link: self.param('link'),
        description: self.param('description'),
        image: ( res.req.url ? res.req.url : '' ),
        modified_at: Date.now()
      }, function(err, doc) {
        if (err)
          throw(err);

        self.redirect('admin/mediapack/');
      });
    });
};

// GET /admin/pages/:id
// Show particular page
MediaPackController.show = function() {
  var self = this;

  MediaPack.findOne({ _id: self.param('id') }, function(err, doc) {
    if (err)
      throw err;

    self.render({
      doc: doc
    });
  });
}

// DELETE /admin/pages/:id
// Process deletion of pages
MediaPackController.destroy = function() {
  var self = this;

  MediaPack.findOne({
    _id: this.param('id')
  }, function(err, document) {
    if (err || !document) {
      self.res.send(500);
    } else {
      document.remove(function(err) {
        if (err) {
          self.res.send(500);
        } else {
          self.res.send(200);
        }
      });
    }
  });
}

// firewall
MediaPackController.before('*', require('../../helpers/firewall').write);

module.exports = MediaPackController;
