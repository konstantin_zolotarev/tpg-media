var locomotive = require('locomotive'),
  Controller = locomotive.Controller;

// base controller
var UsersController = new Controller();

// User model
var User = require('../../models/user');

// GET /admin/users
// List users
UsersController.index = function() {
  var self = this;

  // get pagination variables with defaults
  this.page   = parseInt(this.param('page'), 10) || 1;
  this.limit  = parseInt(this.param('limit'), 10) || 10;

  // make sure these values are safe!
  if (this.page  < 1) this.page  = 1;
  if (this.limit < 1) this.limit = 1;

  // maybe we need to search some stuff?
  this.search_name = this.param('search_name');
  this.search_email = this.param('search_email');

  // sort
  this.sort_by = this.param('sort_by');
  this.order_by = this.param('order_by');

  // filters
  var filter =  {};

  // add a regex for search filter
  if (this.search_name != undefined) {
    filter.name = {
      $regex: this.search_name,
      $options: 'i'
    }
  }

  // add a regex for search filter
  if (this.search_email != undefined) {
    filter.email = {
      $regex: this.search_email,
      $options: 'i'
    }
  }

  // do pagination!
  User.paginate({
    query: filter,

    sort: (this.order_by == 'desc' ? '-' : '') + this.sort_by,

    page:  this.page,
    limit: this.limit
  }, function(err, provider){
    if (err)
      throw err;

    self.render({
      users: provider.docs,
      page: provider.page,
      limit: self.limit,
      pages: provider.pages,
      total: provider.count,
      length: provider.docs.length
    });
  });
}

// GET /admin/users/new
// Create user
UsersController.new = function() {
  this.render();
}

// POST /admin/users
// Process user creation
UsersController.create = function() {
  this.render();
}

// GET /admin/users/:id
// Display user information
UsersController.show = function() {
  var self = this;

  User.findById(this.params('id'), function(err, user) {
    self.updated = self.req.session.updated;
    self.req.session.updated = undefined;

    self.user = user;
    self.render();
  });
}

// GET /admin/users/:id/edit
// Edit a user
UsersController.edit = function() {
  var self = this;

  User.findOne({
//    _id: this.req.user.id
    _id: this.param('id')
  }, function(err, user) {
    if (err || !user) {
      self.redirect('/admin/users');
    } else {

      self.error = self.req.session.error;
      self.req.session.error = undefined;

      self.render({
        user: user
      });
    }
  });
}

// PUT /admin/users/:id
// Process edit
UsersController.update = function() {
  var self = this;
      password = self.param('password');

  User.findOne({
    _id: this.param('id')
  }, function(err, user) {
    if (err) {
      self.redirect('/admin/users');
    } else {

      user.name = self.param('name');
      user.email = self.param('email');
      user.phone = self.param('phone');


      //to provide ability admin to keep user password
      if (typeof password != 'undefined' && password) {
        user.password = password;
      }

      user.authorisation = self.param('authorisation');

      user.validate(function(err) {

        if (err) {
          self.req.session.error = true;
          self.redirect('/admin/users/' + user.id + '/edit');
        } else {
          user.save(function(err) {
            if (err) {
              self.req.session.error = true;
              self.redirect('/admin/users/' + user.id + '/edit');
            } else {
              self.req.session.updated = true;
              self.redirect('/admin/users/' + user.id);
            }
          });
        }
      });
    }
  });
}

// DELETE /admin/users/:id
// Delete user
UsersController.destroy = function() {
  var self = this;

  User.findOne({
    _id: this.param('id')
  }, function(err, document) {
    if (err || !document) {
      self.res.send(500);
    } else {
      document.remove(function(err) {
        if (err) {
          self.res.send(500);
        } else {
          self.res.send(200);
        }
      });
    }
  });
}

// firewall
UsersController.before('*', require('../../helpers/firewall').admin);

module.exports = UsersController;
