var locomotive = require('locomotive')
  , Controller = locomotive.Controller;

var mongoose = require("mongoose")
  , News = require('../../models/news.js')
  , slugify = require('../../helpers/slugify')
  , imgHandler = require('../../helpers/imageHandler');

// base controller
var NewsController = new Controller();

// GET /admin/news
// List all news articles
NewsController.index = function() {
  var self = this;

  // get pagination variables with defaults
  this.page   = parseInt(this.param('page'), 10) || 1;
  this.limit  = parseInt(this.param('limit'), 10) || 10;

  // sort variales
  this.published = this.param('published') || 'all';

  // make sure these values are safe!
  if (this.page  < 1) this.page  = 1;
  if (this.limit < 1) this.limit = 1;

  // maybe we need a search term
  this.search = this.param('search');

  // sort
  this.sort_by = this.param('sort_by') || 'modified_at';
  this.order_by = this.param('order_by') || 'desc';

  // filters
  var filter =  {
    magazines: {
      $all: [ this.app.get('magazine') ]
    }
  };

  // toggle filtered?
  if (this.published != 'all') {
    filter.published = (this.published == 'published' ? true : false);
  }

  // add a regex for search filter
  if (this.search != undefined) {
    filter.title = {
      $regex: this.search,
      $options: 'i'
    }
  }

  // do pagination!
  News.paginate({
    query: filter,

    sort: (this.order_by == 'desc' ? '-' : '') + this.sort_by,

    page:  this.page,
    limit: this.limit
  }, function(err, provider){
    if (err)
      throw err;

    self.render({
      news: provider.docs,
      page: provider.page,
      limit: self.limit,
      pages: provider.pages,
      total: provider.count,
      length: provider.docs.length
    });
  });
}

// GET /admin/news/new
// Create a new news article
NewsController.new = function() {
  // Create and save to work directly with ID on file uploads etc.
  var self = this;

  News.create({
    magazines: [this.app.get('magazine')],
    created_at: Date.now(),
    published: false,
    featured: false
  }, function(err, news) {
    if(err)
      throw(err);

    self.render({
      news: news
    });
  });
}

// GET /admin/news/:id/edit
// Display form for editing news
NewsController.edit = function() {
  // find by id : params('id')
  var self = this;

  // get the news item to edit
  News.findOne({ _id: self.req.params.id }, function(err, news) {
    self.render({
      form_method: 'PUT',
      news: news
    });
  });
}

// PUT /admin/news/:id
// Process news update
NewsController.update = function() {
  var self = this;
  var _file = this.req.client.parser.incoming.files.cover;

  var saveArticle = function(coverImage) {
    var newsObject = {
      title: self.param('title'),
      title_slug: slugify.toURL(self.param('title')),

      content: self.param('content').replace(/^\!\[.*?\]\((.+)\)/, ''),

      category: self.param('category'),
      tags: slugify.slugifyArray(self.param('tags').split(',')),

      published: (self.param('published') == 'yes'),
      featured: (self.param('featured') == 'yes'),
      magazines: ( self.param('magazines') === undefined ? [] : self.param('magazines') ),
      modified_at: Date.now()
    };

    if (self.param('published_at')) {
      newsObject['published_at'] = self.param('published_at');
    }
    if (coverImage) {
      newsObject['header'] = coverImage;
    }

    // save updates
    News.findOneAndUpdate({
      _id: self.param('id')
    }, newsObject, function(err, doc) {
      if (err) {
        throw(err);
      } else {
        self.req.session.updated = true;

        self.redirect( doc.pretty_url() ); // news/:id
      }
    });
  };

  if (_file && _file.originalFilename && _file.originalFilename.length > 0) {
    imgHandler.targetDirectory = "news/"+ self.param('id') +"/";

    imgHandler.uploadFile( _file.path,
      imgHandler.targetDirectory + _file.name,
      function(res) {
        saveArticle(( res.req.url ? res.req.url : '' ));
      });
  } else {
      saveArticle();
  }

}

NewsController.help = function() {
  this.render();
}

// DELETE /admin/news/:id
// Process deletion of news article
NewsController.destroy = function() {
  var self = this;

  News.findOne({
    _id: this.param('id')
  }, function(err, document) {
    if (err || !document) {
      self.res.send(500);
    } else {
      document.remove(function(err) {
        if (err) {
          self.res.send(500);
        } else {
          self.res.send(200);
        }
      });
    }
  });
}

// firewall
NewsController.before('*', require('../../helpers/firewall').write);

// export that shit
module.exports = NewsController;
