var locomotive = require('locomotive')
  , Controller = locomotive.Controller
  , imgHandler = require('../../helpers/imageHandler');

var Testimonial = require('../../models/testimonial');

// new controller
var TestimonialsController = new Controller();


// GET /admin/testimonials
// Show list of all testimonials
TestimonialsController.index = function() {
  var self = this;

  // get pagination variables with defaults
  this.page   = parseInt(this.param('page'), 10) || 1;
  this.limit  = parseInt(this.param('limit'), 10) || 10;

  // make sure these values are safe!
  if (this.page  < 1) this.page  = 1;
  if (this.limit < 1) this.limit = 1;

  var filter =  {};

  Testimonial.paginate({
      query: filter,

      sort: (this.order_by == 'desc' ? '-' : '') + this.sort_by,

      page:  this.page,
      limit: this.limit
  }, function(err, provider){
      if (err)
          throw err;

      self.render({
          docs: provider.docs,
          page: provider.page,
          limit: self.limit,
          pages: provider.pages,
          total: provider.count,
          length: provider.docs.length
      });
  });


//  Testimonial.find({}, function(e, docs){
//    if (e) throw e;
//
//  // Should we remove these from the db here?
////    docs = docs.filter( function(x) { return (x.image && x.image.length > 0) } );
//
//    self.render({
//      docs: docs
//    });
//  });
};

/**
 * Create new testimonials object
 */
TestimonialsController.new = function() {
  var self = this;

  self.render({
      doc: {}
  });
};

// GET /admin/pages/:id/edit
// Edit an Pages
TestimonialsController.edit = function() {
  var self = this;

  Testimonial.findOne({
    _id: self.param('id')
  }, function(err, doc) {
    self.render({
      doc: doc
    });
  });
};

// POST /admin/pages/:id
// Edit an Page
TestimonialsController.update = function() {
  var self = this;
  var _file = this.req.client.parser.incoming.files.filesToUpload;
  var _params = this.req.params;
  if (_file && _file.originalFilename !== '') {
    imgHandler.targetDirectory = "testimonials/"+ _params.id +"/";

    imgHandler.uploadFile( _file.path,
      imgHandler.targetDirectory + _file.name,
      function(res) {

        Testimonial.findOneAndUpdate({
          _id: self.param('id')
        }, {
          title: self.param('title'),
          page: self.param('page'),
          image: ( res.req.url ? res.req.url : '' ),
          modified_at: Date.now()
        }, function(err, doc) {
          if (err)
            throw(err);

          self.redirect('admin/testimonials/');
        });
      });
  } else {
    Testimonial.findOneAndUpdate({
      _id: self.param('id')
    }, {
      title: self.param('title'),
      page: self.param('page'),
      modified_at: Date.now()
    }, function(err, doc) {
      if (err)
        throw(err);

      self.redirect('admin/testimonials/');
    });
  }
};

/**
 * Create new event
 */
TestimonialsController.create = function() {
    var self = this;
    var _file = this.req.client.parser.incoming.files.filesToUpload;
    var _params = this.req.params;

    imgHandler.targetDirectory = "testimonials/"+ _params.id +"/";

    imgHandler.uploadFile( _file.path,
        imgHandler.targetDirectory + _file.name,
        function(res) {

            Testimonial.create({
                title: self.param('title'),
                page: self.param('page'),
                image: ( res.req.url ? res.req.url : '' ),
                modified_at: Date.now()
            }, function(err, doc) {
                if (err)
                    throw(err);

                self.redirect('admin/testimonials/');
            });
        });
};

// GET /admin/pages/:id
// Show particular page
TestimonialsController.show = function() {
  var self = this;

  Testimonial.findOne({ _id: self.param('id') }, function(err, doc) {
    if (err)
      throw err;

    self.render({
      doc: doc
    });
  });
}

// DELETE /admin/pages/:id
// Process deletion of pages
TestimonialsController.destroy = function() {
  var self = this;

  Testimonial.findOne({
    _id: this.param('id')
  }, function(err, document) {
    if (err || !document) {
      self.res.send(500);
    } else {
      document.remove(function(err) {
        if (err) {
          self.res.send(500);
        } else {
          self.res.send(200);
        }
      });
    }
  });
}

// firewall
TestimonialsController.before('*', require('../../helpers/firewall').write);

module.exports = TestimonialsController;
