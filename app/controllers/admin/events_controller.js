var locomotive = require('locomotive')
  , Controller = locomotive.Controller
  , moment = require('moment');

var Event = require('../../models/event');

// new controller
var EventsController = new Controller();

EventsController.index = function() {
  var self = this;

  Event.find({}, function(err, list) {
    if (err) throw err;

    self.render({
      docs: list
    });
  });

};

/**
 * new event
 */
EventsController.new = function() {
  this.render({
    doc: {}
  });
};

/**
 * Find event for edit
 */
EventsController.edit = function() {
  var self = this;

  Event.findById(self.param('id'), function(e, doc) {
    if (e) {
      return self.redirect('admin/events');
    }
    self.render({
      doc: doc
    });
  });
};

/**
 * Show event details
 */
EventsController.show = function() {
  var self = this;

  Event.findById(self.param('id'), function(e, doc) {
    if (e) {
      return self.redirect('admin/events');
    }
    self.render({
      doc: doc
    });
  });
};

/**
 * Update existing event
 */
EventsController.update = function() {
  var self = this;

  Event.findOneAndUpdate({
    _id: self.param('id')
  }, {
    title: self.param('title'),
    date: self.param('date'),
    location: self.param('location') || '',
    city: self.param('city') || '',
    country: self.param('country') || '',
    website: self.param('website') || '',
    event_type: self.param('event_type') || '',
    modified_at: Date.now()
  }, function(err, doc) {
    if (err)
      throw(err);

    self.redirect('admin/events/');
  });
};

/**
 * Create new event
 */
EventsController.create = function() {
  var self = this;
  Event.create({
    title: self.param('title'),
    date: self.param('date'),
    location: self.param('location') || '',
    city: self.param('city') || '',
    country: self.param('country') || '',
    website: self.param('website') || '',
    event_type: self.param('event_type') || ''
  }, function(e, doc) {
    if (e) {
      return self.redirect('admin/events/new');
    }
    //send to edit
    self.redirect('admin/events/'+doc._id+'/edit');
  });
};

/**
 * Remove event
 */
EventsController.destroy = function() {
  var self = this;

  Event.findOne({
    _id: this.param('id')
  }, function(err, document) {
    if (err || !document) {
      self.res.send(500);
    } else {
      document.remove(function(err) {
        if (err) {
          self.res.send(500);
        } else {
          self.res.send(200);
        }
      });
    }
  });
};

// firewall
EventsController.before('*', require('../../helpers/firewall').write);

module.exports = EventsController;
