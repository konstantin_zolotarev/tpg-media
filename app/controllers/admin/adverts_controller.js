var locomotive = require('locomotive')
  , Controller = locomotive.Controller
  , moment = require('moment');

var Advert = require('../../models/advert');

// new controller
var AdvertsController = new Controller();


// GET /admin/adverts
// Show list of all adverts
AdvertsController.index = function() {
  var self = this;

  Advert.find({
    magazines: {
      $all: [ this.app.get('magazine') ]
    }
  }, function(e, docs){
    if (e) throw e;

    // Should we remove these from the db here?
    docs = docs.filter( function(x) { return x.images.length > 0 } );

    self.render({
      ads: docs,
      premium: docs.filter( function(x) { return x.priority >= 1000 && x.priority < 2000 }),
      featured: docs.filter( function(x) { return x.priority >= 2000 && x.priority < 3000 }),
      small: docs.filter( function(x) { return x.priority >= 3000 && x.priority < 4000 }),
      count: docs.length
    });
  });
}

// GET /admin/adverts/:id
// Show particular advert
AdvertsController.show = function() {
  var self = this;

  Advert.findOne({ _id: self.param('id') }, function(err, doc) {
    if (err)
      throw err;

    self.render({
      ad: doc,

      start: moment().subtract('months', 1).format('YYYY-MM-DD'),
      stop:  moment().format('YYYY-MM-DD')
    });
  });
}

// GET /admin/adverts/:id/:type/csv
// Get a CSV of click/impression statistics
AdvertsController.csv = function() {
  var self = this;

  this.id   = this.param('id');
  this.type = this.param('type');

  this.from = this.param('from');
  this.to   = this.param('to');

  Advert.findOne({ _id: self.param('id') }, function(err, document) {
    if (err)
      throw err;

    // data
    var data = {};

    // set data to the correct thing
    switch (self.type) {
      case 'clicks':
        data = document.clicks_by_date;
        break;

      case 'impressions':
        data = document.impressions_by_date;
        break;

      case 'ctr':
        for (var date in document.clicks_by_date) {
          var clicks = document.clicks_by_date[date];
          var impres = document.impressions_by_date[date];

          data[date] = clicks / impres;
        }
        break;

      default:
        break;
    }

    // Fill the gaps
    var start = moment(self.from);
    var stop  = moment(self.to);

    var range = {};

    // don't go too far into the past
    if (start < moment('2013-01-01'))
      start = moment('2013-01-01');

    // don't go into the future
    if (stop > moment())
      stop = moment();

    while (start <= stop) {
      var key = start.format('YYYYMMDD');

      if (data[key] == undefined)
        range[key] = 0;
      else
        range[key] = data[key];

      // increment
      start = start.add('days', 1);
    }

    // render csv
    self.render({
      data: range
    });
  });
}

// GET /admin/adverts/new
// New advert
AdvertsController.new = function() {
  var self = this;

  Advert.create({}, function(err, doc) {
    if(err) throw(err);

    self.render({
      ad: doc
    });
  });
}

// GET /admin/adverts/:id/edit
// Edit an advert
AdvertsController.edit = function() {
  var self = this;

  Advert.findOne({
    _id: self.param('id')
  }, function(err, doc) {
    self.render({
      ad: doc,
      images: doc.images.join(',')
    });
  });
}

// POST /admin/adverts/:id
// Edit an advert
AdvertsController.update = function() {

  var self = this
  , getPriority = function(ft,mob) {

    var weight = 3000;

    if(!!ft) {
      weight -= 1000;

      if(!!mob) weight -= 1000;
    }

    // mobile: 1000-1999
    // featured large: 2000-2999
    // small: 3000-3999
    return weight;
  };

  // save updates
  Advert.findOneAndUpdate({
    _id: this.param('id')
  }, {
    advert_name: this.param('advert_name'),
    link: this.param('link'),
    type: this.param('type') || 'vertical',
    order: this.param('order') || 0,
    images: this.param('image').split(','),
    priority: getPriority( this.param('featured'), this.param('mobile') ),
    magazines: ( this.param('magazines') === undefined ? [] : this.param('magazines') ),
    modified_at: Date.now()
  }, function(err, doc) {
    if (err)
      throw(err);

    self.redirect('admin/adverts/' + doc.id);
  });
}

// DELETE /admin/adverts/:id
// Delete an advert
AdvertsController.destroy = function() {
  var self = this;

  Advert.findOne({
    _id: this.param('id')
  }, function(err, document) {
    if (err || !document) {
      self.res.send(500);
    } else {
      document.remove(function(err) {
        if (err) {
          self.res.send(500);
        } else {
          self.res.send(200);
        }
      });
    }
  });
}

// firewall
AdvertsController.before('*', require('../../helpers/firewall').write);

module.exports = AdvertsController;
