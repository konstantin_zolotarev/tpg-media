var locomotive = require('locomotive')
  , Controller = locomotive.Controller
  , moment = require('moment')
  , imgHandler = require('../../helpers/imageHandler');

var Issue = require('../../models/issue');

// new controller
var IssuesController = new Controller();


// GET /admin/Issues
// Show list of all Issues
IssuesController.index = function() {
  var self = this;
  Issue.find({
//    magazines: {
//      $all: [ this.app.get('magazine') ]
//    }
  }, null, {sort: {created_at: -1}}, function(e, docs){
    if (e) throw e;

    self.render({
      issues: docs,
      count: docs.length
    });
  });
}

// GET /admin/Issues/:id
// Show particular Issue
IssuesController.show = function() {
  var self = this;

  Issue.findOne({ _id: self.param('id') }, function(err, doc) {
    if (err)
      throw err;

    self.render({
      ad: doc,

      start: moment().subtract('months', 1).format('YYYY-MM-DD'),
      stop:  moment().format('YYYY-MM-DD')
    });
  });
}


// GET /admin/Issues/new
// New Issue
IssuesController.new = function() {

  this.render({
    issue: {},
    errors: this.req.flash('error')
  });
}

// GET /admin/Issues/:id/edit
// Edit an Issue
IssuesController.edit = function() {
  var self = this;
  Issue.findOne({
    _id: self.param('id')
  }, function(err, doc) {
    self.render({
      issue: doc,
      errors: self.req.flash('error')
    });
  });
}

// POST /admin/Issues/:id
// Edit an Issue
IssuesController.update = function() {
  var self = this;
  var _file = this.req.client.parser.incoming.files.filesToUpload;
  var _params = this.req.params;

  if (_file && _file.originalFilename !== '') {
    if (_file.headers['content-type'].indexOf('image') !== 0) {
      self.req.flash('error', 'Wrong image file !');
      self.redirect('/admin/issues/'+self.param('id')+'/edit');
      return;
    }
    imgHandler.targetDirectory = "issues/"+ _params.id +"/";

    imgHandler.uploadFile( _file.path,
      imgHandler.targetDirectory + _file.name,
      function(res) {

        // save updates
        Issue.findOneAndUpdate({
          _id: self.param('id')
        }, {
          title: self.param('title'),
          link: self.param('link'),
          image: ( res.req.url ? res.req.url : '' ),
          magazines: ( self.param('magazines') === undefined ? [] : self.param('magazines') ),
          created_at: self.param('created_at') ? self.param('created_at') : Date.now(),
          modified_at: Date.now()
        }, function(err, doc) {
          if (err)
            throw(err);

          self.redirect('admin/issues/');
        });
      });
  } else {
    // save updates
    Issue.findOneAndUpdate({
      _id: self.param('id')
    }, {
      title: self.param('title'),
      link: self.param('link'),
      magazines: ( self.param('magazines') === undefined ? [] : self.param('magazines') ),
      created_at: self.param('created_at') ? self.param('created_at') : Date.now(),
      modified_at: Date.now()
    }, function(err, doc) {
      if (err)
        throw(err);

      self.redirect('admin/issues/');
    });
  }
}

/**
 * Create new event
 */
IssuesController.create = function() {
  var self = this;
  var _file = this.req.client.parser.incoming.files.filesToUpload;
  var _params = this.req.params;

  if (_file.headers['content-type'].indexOf('image') !== 0) {
    self.req.flash('errors', 'Wrong image file !');
    self.redirect('/admin/issues/new');
    return;
  }

  imgHandler.targetDirectory = "issues/"+ _params.id +"/";
  imgHandler.uploadFile( _file.path,
    imgHandler.targetDirectory + _file.name,
    function(res) {

      Issue.create({
        title: self.param('title'),
        link: self.param('link'),
        image: ( res.req.url ? res.req.url : '' ),
        magazines: ( self.param('magazines') === undefined ? [] : self.param('magazines') ),
        modified_at: Date.now(),
        created_at: self.param('created_at') ? self.param('created_at') : Date.now()
      }, function(err, doc) {
        if (err)
          throw(err);

        self.redirect('admin/issues/');
      });
    });
};

// DELETE /admin/Issues/:id
// Delete an Issue
IssuesController.destroy = function() {
  var self = this;

  Issue.findOne({
    _id: this.param('id')
  }, function(err, document) {
    if (err || !document) {
      self.res.send(500);
    } else {
      document.remove(function(err) {
        if (err) {
          self.res.send(500);
        } else {
          self.res.send(200);
        }
      });
    }
  });
}

// firewall
IssuesController.before('*', require('../../helpers/firewall').write);

module.exports = IssuesController;
