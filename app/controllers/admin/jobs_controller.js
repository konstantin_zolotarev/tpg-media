var locomotive = require('locomotive')
  , Controller = locomotive.Controller;

var Job = require('../../models/job');

// new controller
var JobsController = new Controller();

JobsController.index = function() {
  var self = this;

  Job.find({}, function(err, list) {
    if (err) throw err;

    self.render({
      docs: list
    });
  });

};

/**
 * new job
 */
JobsController.new = function() {
  this.render({
    doc: {}
  });
};

/**
 * Find job for edit
 */
JobsController.edit = function() {
  var self = this;

  Job.findById(self.param('id'), function(e, doc) {
    if (e) {
      return self.redirect('admin/jobs');
    }
    self.render({
      doc: doc
    });
  });
};

/**
 * Show event details
 */
JobsController.show = function() {
  var self = this;

  Job.findById(self.param('id'), function(e, doc) {
    if (e) {
      return self.redirect('admin/jobs');
    }
    self.render({
      doc: doc
    });
  });
};

/**
 * Update existing event
 */
JobsController.update = function() {
  var self = this;

  Job.findOneAndUpdate({
    _id: self.param('id')
  }, {
    title: self.param('title'),
    description: self.param('description'),
    email: self.param('email') || '',
    website: self.param('website') || ''
  }, function(err, doc) {
    if (err)
      throw(err);

    self.redirect('admin/jobs/');
  });
};

/**
 * Create new event
 */
JobsController.create = function() {
  var self = this;
  Job.create({
    title: self.param('title'),
    description: self.param('description'),
    email: self.param('email') || '',
    website: self.param('website') || ''
  }, function(e, doc) {
    if (e) {
      return self.redirect('admin/jobs/new');
    }
    //send to edit
    self.redirect('admin/jobs/'+doc._id+'/edit');
  });
};

/**
 * Remove event
 */
JobsController.destroy = function() {
  var self = this;

  Job.findOne({
    _id: this.param('id')
  }, function(err, document) {
    if (err || !document) {
      self.res.send(500);
    } else {
      document.remove(function(err) {
        if (err) {
          self.res.send(500);
        } else {
          self.res.send(200);
        }
      });
    }
  });
};

// firewall
JobsController.before('*', require('../../helpers/firewall').write);

module.exports = JobsController;
