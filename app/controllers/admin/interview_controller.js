var locomotive = require('locomotive')
  , Controller = locomotive.Controller
  , imgHandler = require('../../helpers/imageHandler');

var Interview = require('../../models/interview');

// new controller
var InterviewController = new Controller();


/**
 * list of interviews
 */
InterviewController.index = function() {
  var self = this;
  Interview.find({}, function(e, list) {
    if (e) throw e;

    self.render({
      docs: list
    });
  });
};

/**
 * Create new one
 */
InterviewController.new = function() {
  this.render({
    doc: {}
  })
};

/**
 * Edit interview
 */
InterviewController.edit = function() {
  var self = this;
  Interview.findById(self.param('id'), function(e, doc) {
    if (e) throw e;

    self.render({
      doc: doc
    });
  });
};

/**
 * show interview details
 */
InterviewController.show = function() {
  var self = this;
  Interview.findById(self.param('id'), function(e, doc) {
    if (e) throw e;

    self.render({
      doc: doc
    });
  });
};

/**
 * Create new interview
 */
InterviewController.create = function() {
  var self = this;
  var _file = this.req.client.parser.incoming.files.cover;

  this.req.assert('title').notEmpty();
  if (this.req.body.video) {
    this.req.assert('video').isUrl();
  }
  if (this.req.validationErrors()) {
    console.log(this.req.validationErrors());
    return self.redirect('/admin/interview/new');
  }

  function saveInterview(cover) {
    Interview.create({
      title: self.param('title'),
      cover: cover || '',
      video: self.param('video') || '',
      content: self.param('video') ? '' : (self.param('content') || ''),
      description: self.param('description') || ''
    }, function(e, doc) {
      if (e) throw e;

      self.redirect('/admin/interview/'+doc._id+'/edit');
    });
  }

  if (_file && _file.originalFilename && _file.originalFilename.length > 0) {
    imgHandler.targetDirectory = "interview/"+ self.param('id') +"/";

    imgHandler.uploadFile( _file.path,
      imgHandler.targetDirectory + _file.name,
      function(res) {
        saveInterview(( res.req.url ? res.req.url : '' ));
      });
  } else {
    saveInterview();
  }
};

/**
 * Update interview
 */
InterviewController.update = function() {
  var self = this;
  var _file = this.req.client.parser.incoming.files.cover;

  this.req.assert('title').notEmpty();
  if (this.req.body.video) {
    this.req.assert('video').isUrl();
  }
  if (this.req.validationErrors()) {
    console.log(this.req.validationErrors());
    return self.redirect('/admin/interview/'+self.param('id')+'/edit');
  }

  function saveInterview(cover) {
    var obj = {
      title: self.param('title'),
      video: self.param('video') || '',
      content: self.param('video') ? '' : (self.param('content') || ''),
      description: self.param('description') || ''
    };
    if (cover) {
      obj.cover = cover;
    }
    Interview.findOneAndUpdate({
      _id: self.param('id')
    }, obj, function(e, doc) {
      if (e) throw e;

      self.redirect('/admin/interview/');
    });
  }

  if (_file && _file.originalFilename && _file.originalFilename.length > 0) {
    imgHandler.targetDirectory = "interview/"+ self.param('id') +"/";

    imgHandler.uploadFile( _file.path,
      imgHandler.targetDirectory + _file.name,
      function(res) {
        saveInterview(( res.req.url ? res.req.url : '' ));
      });
  } else {
    saveInterview();
  }
};

/**
 * Remove interview
 */
InterviewController.destroy = function() {
  var self = this;
  Interview.findById(self.param('id'), function(e, doc) {
    if (e) {
      return self.res.send(500);
    }
    doc.remove(function(e) {
      if (e) {
        return self.res.send(500);
      }
      self.res.send(200);
    });
  });
};

// firewall
InterviewController.before('*', require('../../helpers/firewall').write);

module.exports = InterviewController;
