var locomotive = require('locomotive'),
  Controller = locomotive.Controller;

// base controller
var AdminController = new Controller();

// GET /admin
// Admin control panel
AdminController.index = function() {
  this.render();
}

// at *least* write permission
AdminController.before('*', require('../../helpers/firewall').write);

module.exports = AdminController;
