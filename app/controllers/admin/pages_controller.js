var locomotive = require('locomotive')
  , Controller = locomotive.Controller
  , imgHandler = require('../../helpers/imageHandler');

var PageConfig = require('../../models/pageConfig');

// new controller
var PagesController = new Controller();


// GET /admin/pages
// Show list of all pages
PagesController.index = function() {
  var self = this;

  PageConfig.find({

  }, function(e, docs){
    if (e) throw e;

    // Should we remove these from the db here?
    docs = docs.filter( function(x) { return (x.title && x.title.length > 0) } );

    self.render({
      pages: docs
    });
  });
};

/**
 * Create new page config object
 */
PagesController.new = function() {
  var self = this;

  PageConfig.create({}, function(err, pageConfig) {
    if(err)
      throw(err);

    self.render({
      page: pageConfig
    });
  });
};

// GET /admin/pages/:id/edit
// Edit an Pages
PagesController.edit = function() {
  var self = this;

  PageConfig.findOne({
    _id: self.param('id')
  }, function(err, doc) {
    self.render({
      page: doc
    });
  });
};

// POST /admin/pages/:id
// Edit an Page
PagesController.update = function() {
  var self = this;
  var _file = this.req.client.parser.incoming.files.filesToUpload;
  var _params = this.req.params;

  imgHandler.targetDirectory = "pages/"+ _params.id +"/";

  imgHandler.uploadFile( _file.path,
    imgHandler.targetDirectory + _file.name,
    function(res) {

      PageConfig.findOne({
        _id: {
          '$ne': self.param('id')
        },
        page: self.param('page')
      }, function(err, page) {
        if (err) throw err;

        if (!page) {
          // save updates
          PageConfig.findOneAndUpdate({
            _id: self.param('id')
          }, {
            title: self.param('title'),
            page: self.param('page'),
            image: ( res.req.url ? res.req.url : '' ),
            modified_at: Date.now()
          }, function(err, doc) {
            if (err)
              throw(err);
          });
        }
        self.redirect('admin/pages/');
      });
    });
};

// GET /admin/pages/:id
// Show particular page
PagesController.show = function() {
  var self = this;

  PageConfig.findOne({ _id: self.param('id') }, function(err, doc) {
    if (err)
      throw err;

    self.render({
      page: doc
    });
  });
}

// DELETE /admin/pages/:id
// Process deletion of pages
PagesController.destroy = function() {
  var self = this;

  PageConfig.findOne({
    _id: this.param('id')
  }, function(err, document) {
    if (err || !document) {
      self.res.send(500);
    } else {
      document.remove(function(err) {
        if (err) {
          self.res.send(500);
        } else {
          self.res.send(200);
        }
      });
    }
  });
}

// firewall
PagesController.before('*', require('../../helpers/firewall').write);

module.exports = PagesController;
