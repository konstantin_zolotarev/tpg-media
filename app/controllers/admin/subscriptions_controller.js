var locomotive = require('locomotive')
  , Controller = locomotive.Controller
  , Paymill = require('../../helpers/paymill').client;

var SubscriptionsController = new Controller();

// GET /admin/subscriptions
SubscriptionsController.index = function() {
  var self = this;


  Paymill.offers.list( {}, function(err, offer) {
    if(err) throw err;

    var doms = {}; // Want global array <<<

    ['emirates','kuwait','qatar','oman','saudi'].forEach( function(a) {
      doms[a] = offer.data.filter( function(x) { return x.name.match( a ) });
    });

    return self.render({
      paymill_public_key: Paymill.public_key,
      offers: doms
    });

  });

}

// GET /admin/subscriptions
SubscriptionsController.show = function() {
  var self = this;

  Paymill.offers.details( self.param('id'), function(err, offer) {
    if(err) throw err;

    return self.render({
      paymill_public_key: Paymill.public_key,
      offer: offer
    });
  });
}

// GET /admin/subscriptions
SubscriptionsController.new = function() {
  var self = this;

  console.log( this.param('domain') );
  return self.render({
    domain: this.param('domain'),
    domains: ['emirates','kuwait','qatar','oman','saudi']
  });
}

// GET /admin/subscriptions
SubscriptionsController.edit = function() {
  var self = this;

  Paymill.offers.details( self.param('id'), function(err, offer) {
    if(err) throw err;

    console.log(offer.data.name);

    return self.render({
      paymill_public_key: Paymill.public_key,
      domain: offer.data.name,
      domains: ['emirates','kuwait','qatar','oman','saudi'],
      offer: offer
    });
  });
}


// GET /admin/subscriptions
SubscriptionsController.create = function() {
  var self = this;
  Paymill.offers.create({
    name: this.param('name'),
    currency:"GBP",
    interval: this.param('interval'),
    amount: parseInt( this.param('amount') * 100 )

  }, function(err,offer) {
    return self.redirect('admin/subscriptions');
  })
}

// GET /admin/subscriptions
SubscriptionsController.update = function() {
  var self = this;
  Paymill.offers.update( self.param('id'), {
    name: this.param('name'),
    currency:"GBP",
    interval: this.param('interval'),
    amount: parseInt( this.param('amount') * 100 )

  }, function(err,offer) {
    return self.redirect('admin/subscriptions');
  })
}

// GET /admin/subscriptions
SubscriptionsController.destroy = function() {
  var self = this;

  Paymill.offers.remove( self.param('id'), function(err, offer) {
    if(err) throw err;

    return self.redirect('/admin/subscriptions');

  });
}


module.exports = SubscriptionsController;
