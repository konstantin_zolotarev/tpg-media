var locomotive = require('locomotive')
  , Controller = locomotive.Controller;

var Advert = require('../models/advert');

// new controller
var AdvertsController = new Controller();

// GET /adverts
// Get advert to display
AdvertsController.index = function() {
  var self = this;

  Advert.find( {
    magazines: {
      $all: [ this.app.get('magazine') ]
    }
  }, null, {sort: {order: 1}}, function(err, docs) {
    if (err)
      throw err;

    // results
    docs = docs.filter( function(x) { return x.images.length > 0 } );

    var horizontal = docs.filter( function(x) { return x.type == 'horizontal' });
    var featured = docs.filter( function(x) { return x.priority >= 2000 && x.priority < 3000 });
    var small    = docs.filter( function(x) { return x.priority >= 3000 && x.priority < 4000 });

    // shuffle function - commented to provide adverts order according Order field
//    var shuffle = function() { return Math.round(Math.random() - 0.5) };
//
//    // shuffle these arrays
//    horizontal.sort(shuffle);
//    featured.sort(shuffle);
//    small.sort(shuffle);

    self.res.json( {
      horizontal: horizontal.splice(0, 2),
      featured: featured.splice(0, 3),
//      small: small.splice(0, 6) //commented to make all adverts display on the pages in right order
      small: small
    });
  });
};

// GET /advert/:id
// 'Display' advert and redirect to target
AdvertsController.show = function() {
  var self = this;

  Advert.findOne({
    _id: self.params('id')
  }, function(err, document) {
    if (err || !document) {
      self.res.send(200);
    } else {
      // call incr click
      document.add_click(function() {
        // redirect to the link
        self.redirect( document.link );
      });
    }
  });
}

// GET /capture/adverts/:id
// Record impression
AdvertsController.impression = function() {
  var self = this;

  // get the advert document
  Advert.findOne({
    _id: self.params('id')
  }, function(err, document) {
    if (err || !document) {
      self.res.send(200);
    } else {
      // call incr impression
      document.add_impression(1, function(err) {
        self.res.json((!!err) ? {error: true} : {success: true});
      });
    }
  });
}

module.exports = AdvertsController;
