var locomotive = require('locomotive')
  , Controller = locomotive.Controller;

var MediaPack = require('../models/mediapack');

// new controller
var MediaPackController = new Controller();

// GET /adverts
// Get advert to display
MediaPackController.index = function() {
  var self = this;

  MediaPack.find( {
//    magazines: {
//      $all: [ this.app.get('magazine') ]
//    }
  }, null, {limit: 20}, function(err, docs) {
    if (err)
      throw err;

    self.render({
      docs: docs
    });
  });
};

module.exports = MediaPackController;
