var locomotive = require('locomotive')
, Controller = locomotive.Controller;

var mongoose = require("mongoose")
, News = require('../models/news.js')
, Slugify = require('../helpers/slugify.js');

var async = require('async');

var categories = require('../../config/app').categories

var NewsController = new Controller();

// GET /news
// GET /news/:category
// List recent news
NewsController.index = function() {
  var self = this;

  var tasks = []
    , newsByCategory = {};

  for ( var i = categories.length - 1; i >= 0; i--){
    (function(catName){
      tasks.push(function(callback) {
        News.find({
          category: catName,
          magazines: { $all: [ self.app.get('magazine') ] },
          published: true
        }
        , null
        , { sort: { 'published_at': -1 }, limit: 2 }
        , function(err, data){
            if (err){
              callback(err);
            }

            for (var i = 0, len = data.length; i < len; i++) {
              if (!newsByCategory[data[i].category]) {
                newsByCategory[data[i].category] = [data[i]];
              } else {
                newsByCategory[data[i].category].push(data[i]);
              }
            }
            callback();
          });
      });
    })(categories[i]);
  }

  async.parallel(
    tasks,
    function(err, results) {

      //prepare categories titles object
      var news_by_category = function() {
        var by_cat = {};
        for (var i = categories.length - 1; i >= 0; i--) {
          by_cat[categories[i]] = Slugify.unSlugify(categories[i])
        };

        return by_cat;
      };


      // get featured news
      var featuredNews = []
        , featuredNewsTitles = [];
      News.find({
        magazines: { $all: [ self.app.get('magazine') ] },
        published: true,
        featured: true
      }
        , null
        , { sort: { 'published_at': -1}, limit: 2 }
        , function(err, news) {
          if (typeof news != "undefined") {
            featuredNews = news;
            featuredNewsTitles = news.map( function(e){ return e.title; } );
          }

          self.respond({
            html: function() {
              self.render({
                title: 'TPG',
                categories: news_by_category(),
                news: newsByCategory,
                featured_news: featuredNews,
                breaking_news: featuredNews
              })
            },
            json: function() { self.res.json( featuredNewsTitles ); }
          });
        }
      );

    }
  );
}

NewsController.byCategory = function() {
  var self = this;

  // get pagination variables with defaults
  this.page   = parseInt(this.param('page'), 10) || 1;
  this.limit  = parseInt(this.param('limit'), 10) || 25;

  // make sure these values are safe!
  if (this.page  < 1) this.page  = 1;
  if (this.limit < 1) this.limit = 1;

  // category filter
  this.category = this.param('category');

  var filter = {
    magazines: { $all: [ this.app.get('magazine') ] },
    published: true
  };

  if (this.category != undefined)
    filter.category = this.category;

  // do pagination!
  News.paginate({
    query: filter,

    page:  this.page,
    sort: { published_at: -1 },
    limit: this.limit
  }, function(err, provider){
    if (err)
      throw err;

    news_by_category = {}
    news_by_category = provider.docs.map( function(e) {
      if( news_by_category[e.category] === undefined ) {
        news_by_category[e.category] = [];
      } else {
        news_by_category[e.category].push(e);
      }
      return e;
    });

    self.respond({
      json: function() { self.res.json( provider.docs.map(function(e) { return e.title; }).slice(0,2) ); },
      // json: function() { self.res.json( provider.docs ); },
      html: function() {
        self.render({
          category_news: news_by_category,
          news: provider.docs,
          page: provider.page,
          limit: self.limit,
          pages: provider.pages,
          total: provider.count,
          length: provider.docs.length
        });
      }
    })
  });
}

// POST /news
// Process new news article
NewsController.create = function() {
  this.redirect('news'); // news/:id
}

// GET /news/:yyyy/:mm/:dd/:title
// Display a news article
NewsController.show = function() {
  var self = this;
  var magazine = this.app.get('magazine');

  var renderArticle = function(err, article) {
    // error?
    if (err || article == null) {
      // REDIRECT!
      self.redirect('/news');
    } else {
      // updated?
      self.updated = self.req.session.updated;
      self.req.session.updated = undefined;

      var getNextItem = function(article, magazine, callback) {
        News.findOne({
          magazines: { $all: [ magazine ] },
          category: article.category,
          published_at:{
            $gte: article.published_at
          },
          _id: {
            $ne: article._id
          }
        }
        , callback);
      };

      getNextItem(article, magazine, function(err, item){
        // Render that shit.
        self.render({ news: article, nextItem: item, html_content: article.rendered_content() });
      });


    }
  };

  // Find based on URL.
  if( self.req.params.yyyy ) {

    News.findOne({
      magazines: { $all: [ this.app.get('magazine') ] },

      published_at: {
        $gte: new Date(self.req.params.yyyy, parseInt(self.req.params.mm, 10) - 1, parseInt(self.req.params.dd, 10) ),
        $lte: new Date(self.req.params.yyyy, parseInt(self.req.params.mm, 10) - 1, parseInt(self.req.params.dd, 10) + 1)
      },
      title_slug: self.req.params.title

    }, renderArticle );

  } else {
    News.findOne({
      magazines: { $all: [ this.app.get('magazine') ] },
      _id: self.req.params.id
    }, renderArticle );
  }
}

// only subscribers can read articles
//NewsController.before('show', require('../helpers/firewall').subscribed);

module.exports = NewsController;
