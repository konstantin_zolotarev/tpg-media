var locomotive = require('locomotive')
  , Controller = locomotive.Controller;

var Interview = require('../models/interview');
var InterviewsController = new Controller();

// GET /interviews
// List of interviews
InterviewsController.index = function() {
    var self = this;
    Interview.find({}, function(e, list) {
        if (e) throw e;
        self.render({
            interviews: list
        });
    });
};

// GET /interviews/:id
// Show interview details
InterviewsController.show = function() {
  var self = this;
  Interview.findById(self.req.params.id, function(e, doc) {
    if(e) throw e;

    var videoPath;
    // render video path to get video code
    if (doc && doc.video) {
      videoPath = 'https://www.youtube.com/v/' + youtubeParser(doc.video) + '?enablejsapi=1&playerapiid=ytplayer1&version=3';
    }

    self.render({
      interview: doc,
      videoPath: videoPath
    });
  });
};

module.exports = InterviewsController;

function youtubeParser(url){
  var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
  var match = url.match(regExp);
  if (match&&match[7].length==11){
    return match[7];
  }else{
    return null;
  }
}
