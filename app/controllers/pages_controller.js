var locomotive = require('locomotive')
, Controller = locomotive.Controller
, moment = require('moment')

, News = require('../models/news')
, Interview = require('../models/interview')
, Event = require('../models/event')
, Advert = require('../models/advert')
, Issue = require('../models/issue')
, Testimonial = require('../models/testimonial')
, imghandler = require('../helpers/imageHandler')
, request = require('request')

var PageImage = require('../helpers/pageImage');
var PagesController = new Controller();


// GET /
// Display the index page
PagesController.index = function() {
  var self = this;
  var getNews = function(cb) {
    return News.find({
      magazines: { $all: [ self.app.get('magazine') ] },
      published: true
    }).sort({'published_at': -1}).execFind( cb )

  }, getVideos = function(cb) {
    return Interview.find({
//      magazines: { $all: [ self.app.get('magazine') ] }
    }).sort({'created_at': -1}).limit(1).execFind( cb )

  }, getIssues = function(cb) {
    return Issue.find({
      magazines: { $all: [ self.app.get('magazine') ] }
    }).sort({'created_at': -1}).limit(1).execFind( cb )

  }, getEvents = function(cb) {
    return Event.find({
      date: {
        '$gte': moment().startOf('day').format(),
        '$lt': moment().endOf('day').format()
      }
    }, cb);
  }

  getNews( function(err,news) {
    getVideos( function( err, videos) {
      getIssues ( function(err, issues) {
        getEvents( function(err, events) {

          var featuredNews = news.filter( function(e) { if(e.featured == true) return e });
          self.render({
            title: 'TPG',
            news: news.filter( function(e) { if(e.featured !== true) return e }).slice(0, 15),
            featured_news: featuredNews.slice(0, 1),
            breaking_news: featuredNews,
            videos: videos,
            issues: issues,
            events: events,
            notices: self.req.flash('notice'),
            errors: self.req.flash('error'),
            successes: self.req.flash('success')
          });
        })
      })
    })
  })
}



PagesController.upload = function() {
  var self = this
  , _file = this.req.client.parser.incoming.files.file
  , _args = this.req.body
  , _params = this.req.params

  switch(_params.model) {
    case "news" :
    imghandler.targetDirectory = "news/"+ _params.id +"/";

    imghandler.uploadFile( _file.path,
      imghandler.targetDirectory + _file.name,
      function(res) {
        self.respond({ json: function() {
          return self.res.json({ url: res.req.url }) }
        });
      })

    break;

    case "ad" :
      var sizes;
      if (_args.type == 'horizontal') {
        sizes = [{
          width:728,
          height:90
        }];
      } else {
        sizes = [{
          width:250,
          height:250
        }];
//        if( _args.featured == "true" ) {
//          sizes = [{
//            width:600,
//            height:500,
//            retina:true
//          }, {
//            width:300,
//            height:250
//          }];
//        } else {
//          sizes = [{
//            width:400,
//            height:400,
//            retina:true
//          }, {
//            width:200,
//            height:200
//          }];
//        }
      }
      imghandler.targetDirectory = "ads/"+ _params.id +"/";
        //@todo prepare not working
      imghandler.prepare( _file, sizes, function(files) {
        imghandler.uploadFiles( files.map( function(f) {
          return { file: f, path: imghandler.targetDirectory }
        }), function(res) {
          self.respond({ json: function() {
            return self.res.json({ url: res }) }
          });
        })
      });

    break;

    default :
    break;
  }
}

PagesController.weather = function() {
  var self = this;
  var api = 'ae749f6d062738d1a8a8beda0e3796a9';
  var defaultPos = {
      latitude: '25.250',
      longitude: '55.330'
  };

  var lat = self.param('lat') || defaultPos.latitude;
  var long = self.param('long') || defaultPos.longitude;

  request("https://api.forecast.io/forecast/"+ api + "/" + lat + "," + long + "?units=si", function (error, response, body) {
    var obj = JSON.parse(body);

    if (!error) {
      self.render({
        timezone: obj.timezone,
        temp: Math.round(obj.currently.temperature),
        icon: obj.currently.icon,
        summary: obj.currently.summary
      })
    }
  })

}


PagesController.redirect = function() {
  var self = this, params = this.req.params;

  switch(params.module) {
    case "ad" :
    Advert.find({ id: params.id }, function(e, doc) {
      doc.clicked( function(ele) {
        self.redirect( ele.link );
      })
    });

    default :
    return false;
  }
}


// GET /advertise
// Advertise with us page
PagesController.advertise = function() {
  var self = this;

  this.render();
}

// GET /contact
// Display contact form
PagesController.contact = function() {
  var self = this;
  PageImage('contactus', function(e, img) {
    self.render({
      image: img || false
    });
  });
}

// GET /free-features
// Show the free features page
PagesController.freeFeatures = function() {
  var self = this;

  this.render();
}

// GET /subscribe
// Process subscriptions
PagesController.subscribe = function() {
  var self = this;

  return self.render({
    offers: ['emirates','bahrain','kuwait','oman','qatar','saudi'],
    error: self.param('error') || false
  });

  // paymill -- currently disabled - Emirates has no paypal and paymill
//  var Paymill = require('../helpers/paymill');
//  Paymill.client.offers.list( {}, function(err, data) {
//    if(err) throw err;
//
//    var doms = {}; // Want global array <<<
//    ['emirates','bahrain','kuwait','qatar','saudi'].forEach( function(a) {
//      doms[a] = data.data.filter( function(x) { return x.name.match( a ) });
//    });
//
//    return self.render({
//      paymill_public_key: Paymill.public_key,
//      offers: doms,
//      error: self.param('error') || false
//    });
//
//  });
}

// POST/subscription
// params: payment_token, [offers_selected]
PagesController.newSubscription  = function() {
  var current_user = this.req.user
  , self = this
  , _args = this.req.body
  , Paymill = require('../helpers/paymill').client;

  if(!current_user) return false;

  // Filter selections for duplicates.
  var current_subscriptions = current_user.subscriptions.map( function(x) { return x.magazine });

  Paymill.payments.create({
    token: _args.paymillToken,
    client: current_user.client_id
  }, function(err, payment) {
    if(err) throw err;

    var keys = Object.keys(_args.offers).filter( function(x) { return current_subscriptions.indexOf(x) < 0 } )
    , iterator = keys.length;


    keys.forEach( function(key) {
      Paymill.subscriptions.create( {
        client: current_user.client_id,
        offer: _args.offers[key],
        payment: payment.data.id
      }, function(err, sub) {
        if(err) {
          console.log(err);
          return;
        }

        current_user.subscriptions.push({
          magazine: key,
          subscription_id: sub.data.id,
          start_date: Date(sub.data.created_at),
          end_date: Date(sub.data.next_capture_at)
        });

        if( --iterator <= 0 ) {
          current_user.save(function(e) {
            self.req.flash('success', 'Your subscriptions have been added.' );
            self.res.redirect( '/' );
          });
        }
      });

    });

  } );
}

// GET /testimonials
// Show testimonials
PagesController.testimonials = function() {
  var self = this;
  PageImage('testimonials', function(e, img) {
    Testimonial.find({}, function(err, list) {
      self.render({
        testimonials: list,
        image: img || false
      });
    });
  });
}

/**
 * Ability to download magazine
 */
PagesController.downloadissue = function() {
  var self = this;
  var id = self.param('id');
  if (!id) {
    self.res.redirect('/');
  }
  Issue.findById(id, function(err, doc) {
    if (err || !doc.link) {
      self.res.redirect('/');
    }
    self.res.redirect(doc.link);
  });
}

// POST /message
// Deal with form data to send contact mail
PagesController.message = function() {
  var self = this;

  // mail stuff
  var tpg    = require('../../config/app');
  var mailer = require('../helpers/mailer');

  // params
  var name = this.param('name');
  var email = this.param('email');
  var message = this.param('message');
  var desc = this.param('desc') || 'Query';

  // validate input
  this.req.assert('name', 'A valid name is required').notEmpty();
  this.req.assert('email', 'A valid email address is required').isEmail();
  this.req.assert('message', 'A valid message is required').notEmpty();

  var errors = this.req.validationErrors();

  if (errors) return this.res.send(500);

  // send mail
  mailer.send('contact', {
//    from: mailer.format(name, email),
    from: mailer.format(tpg.mail.sender.name,   tpg.mail.sender.address),
//    from: tpg.mail.sender.address,
    to:   mailer.format(tpg.mail.contact.name,  tpg.mail.contact.address),
//    to:   tpg.mail.contact.address,
    cc:   tpg.mail.contactCC.addresses,

    subject: this.app.get('name') + ' - ' + desc + ' - ' + name
  }, {
    desc: desc,
    name: name,
    email: email,
    message: message
  });

  // return okay
  this.res.send(200);
}

// force registration
PagesController.before([/*'subscribe', */'newSubscription', 'downloadissue'], require('../helpers/firewall').none);

module.exports = PagesController;
