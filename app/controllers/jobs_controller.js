var locomotive = require('locomotive')
  , Controller = locomotive.Controller;

var Job = require('../models/job');

var JobsController = new Controller();

// GET /events
// List events
JobsController.index = function() {
  var self = this;

  Job.find({}, function(e, list) {
    if (e) throw e;

    self.render({
      jobs: list
    });
  });
};

JobsController.before('*', require('../helpers/firewall').none);

module.exports = JobsController;
