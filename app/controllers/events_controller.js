var locomotive = require('locomotive')
  , Controller = locomotive.Controller;

var Event = require('../models/event'),
  moment = require('moment');

var EventsController = new Controller();

// GET /events
// List events
EventsController.index = function() {
  var self = this;

  Event.find({
    date: {
      '$gte': moment().startOf('day').format()
    }
  }, null, {
    order: {
      date: 1,
      title: 1
    }
  }, function(e, list) {
    if (e) throw e;

    self.render({
      events: list
    });
  });
};

// GET /events/:id
// Show event details
EventsController.show = function() {
  this.render();
};

module.exports = EventsController;
