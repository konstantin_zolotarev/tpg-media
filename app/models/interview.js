var mongoose = require('mongoose');
var markdown = require( "markdown" ).markdown;

var InterviewSchema = new mongoose.Schema({
  title: String,
  cover: String,
  video: String,
  content: String,
  description: String,

  created_at: Date,
  modified_at: Date
});


//set indexes
InterviewSchema.index({
  title: 1,
  video: 1,
  content: 'text'
});

var presave = require('../helpers/presave');

// get markdown as rendered html
InterviewSchema.method('rendered_content', function() {
  return markdown.toHTML(this.content).replace(
    /<p><img alt=\"(.*?)\" src=\"(.*?)\"\/><\/p>/,
    function(match,alt,url) {
      return "<figure><figcaption>"+alt+"</figcaption><img alt='"+alt+"' src='"+url+"' /></figure>";
      // "
    }
  );
});

// get markdown as rendered html
InterviewSchema.method('isVideo', function() {
  return Boolean(this.video);
});

InterviewSchema.pre('save', function(next) {
  // presave helper
  presave.update(next);
  if (!this.created_at)
    this.created_at = new Date();
});

module.exports = mongoose.model('Interview', InterviewSchema);