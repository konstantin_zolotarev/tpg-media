var mongoose = require('mongoose')
  , Advert = require('../models/advert.js');

// Schema data
var AdvertiserSchema = new mongoose.Schema({
  name: String,
  contact: String,
  notes: String,

  logo: String,
  created_at: Date,
  modified_at: Date
});

var presave = require('../helpers/presave');
AdvertiserSchema.pre( 'save', function(next) { presave.update(next) } );

AdvertiserSchema.method('adverts', function() {
  var self = this;
  Advert.find({ advertiser_id: self.id }, function(e,doc) {
    if(e) throw e;
    return doc;
  })
});


module.exports = mongoose.model('Advertiser', AdvertiserSchema);
