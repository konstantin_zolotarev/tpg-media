var mongoose = require('mongoose')
  , slugify = require('../helpers/slugify')
  , textSearch = require('mongoose-text-search')
  , utilities = require('mongoose-utilities')
  , markdown = require( "markdown" ).markdown;

var NewsSchema = new mongoose.Schema({
  title: String,
  title_slug: String,
  header: String,

  content: String,

  tags: [String],
  category: String,
  magazines: [String],

  published: Boolean,
  featured: Boolean,
  user_id: Number,

  restricted: {
    type: Boolean,
    default: false
  },

  created_at: Date,
  modified_at: Date,
  published_at: Date
});

// load textSearch plugin
NewsSchema.plugin(textSearch);

// load paginate plugin
NewsSchema.plugin(utilities.pagination);

// set index
NewsSchema.index({
  title: 'text',
  content: 'text',
  tags: 'text'
});

// set appropriate created/edited dates
var presave = require('../helpers/presave');
NewsSchema.pre( 'save', function(next) {
  var news = this;

  // if published state has changed update the published_at date
  if (news.isModified('published') && !news.isModified('published_at'))
    news.published_at = new Date();

  // presave helper
  presave.update(next)
});


// used in the templates to grab a nice url
NewsSchema.method('pretty_url', function() {
  var pub = this.published_at;

  return '/news/' + pub.getFullYear() +'/'+ (pub.getMonth()+1) +'/'+ pub.getDate() +'/'+ this.title_slug;
});

// used in the templates to grab a nice url
NewsSchema.method('publish_date', function() {
    return this.published_at.getDate() +' '+ this.published_at.getMonthName() +' '+ this.published_at.getFullYear();
});

// used in the templates to grab a nice url
NewsSchema.method('edit_path', function() {
  return '/admin/news/' + this.id + '/edit'
});

// set boolean of restricted
NewsSchema.method('restrict', function(bool, callback) {
  if( typeof(bool) != 'boolean' )
    throw 'Non-boolean argument given';

  this.restricted = bool;

  return callback(bool, this);
});

// get markdown as rendered html
NewsSchema.method('rendered_content', function() {
  return markdown.toHTML(this.content).replace(
    /<p><img alt=\"(.*?)\" src=\"(.*?)\"\/><\/p>/,
    function(match,alt,url) {
      return "<figure><figcaption>"+alt+"</figcaption><img alt='"+alt+"' src='"+url+"' /></figure>";
      // "
    }
  );
//  .nl2br();
});

// return small amount of text for use as a preview
NewsSchema.method('excerpt', function(length){
  return markdown.toHTML( this.content
    // [link text](http://lol.com) -> [link text]
    .replace(/\(.+\)/g, '')

    // remove common formatting markdown chars
    .replace(/[*_\[\]\(\)#\>\<]/g, '')

    // truncate
    .substr(0, length)

    // try to take to the end of the last whole word
    .replace(/\s[\w\d]+$/, "") + '…' )
    .nl2br();
});

// get images
NewsSchema.method('images', function() {
  var self = this;

  var pattern = new RegExp(/\!\[[^\]]+\]\(([^\)]+)\)/gi);
  var images = [];
  var match;

  while(match = pattern.exec(this.content))
    images.push(match[1]);

  return images;
});

// used in templated to get a nice date
NewsSchema.method('pretty_date', function(date){
  if (date === undefined)
    return '';

  return date.getDate() + "<sup>" + date.getOrdinal() + "</sup> <span class='long-month'>" + date.getMonthName() + "</span><span class='short-month'>" + date.getMonthShortName() + "</span> <span class='year'>" + date.getFullYear() + "</span>";
});

// used in templates to get a nice category
NewsSchema.method('pretty_category', function(category){
  if (category === undefined)
    category = this.category;

  if (category === undefined)
    return '';

  return slugify.unSlugify(category);
});

module.exports = mongoose.model('News', NewsSchema);
