var mongoose = require('mongoose');

var SubscriptionSchema = new mongoose.Schema({
  user: String,
  option: Number,
  start_date: Date,
  end_date: Date,
  payment_method: String
});
