var mongoose = require('mongoose')
  , Advertiser = require('../models/advertiser.js')
  , moment = require('moment');

// Schema data

// Can't nest correctly.  Dumb.
var ClickImpressionSchema = new mongoose.Schema({
    count: { type: Number, default: 0 },
    by_date: { type: Object, default: { } },
    previous: { type: Date, default: null }
});

var AdvertSchema = new mongoose.Schema({
  link: String,
  advert_name: { type: String, default: '' },
  images: [String],

  type: {type: String, default: 'vertical'},
  order: { type: Number, default: 0 },

  regularity: Number,
  priority: { type: Number, default: 3999 },

  published: Boolean,
  advertiser_id: Number,

  magazines: [String],

  clicks_count: { type: Number, default: 0 },
  clicks_by_date: { type: mongoose.Schema.Types.Mixed, default: {} },
  clicks_last: Date,

  impressions_count: { type: Number, default: 0 },
  impressions_by_date: { type: mongoose.Schema.Types.Mixed, default: {} },
  impressions_last: Date,

  created_at: Date,
  published_at: Date,
  modified_at: Date
});


var presave = require('../helpers/presave')

// before saving
AdvertSchema.pre('save', function(next) {
  presave.update(next)
});

// get the advertiser
AdvertSchema.method('advertiser', function() {
  var self = this;

  Advertiser.findOne({
    id: self.id
  }, function(e, doc) {
    if (e)
      throw e;
    else
      return doc;
  });
});

// incr impression
AdvertSchema.method('add_impression', function(impressions, callback) {
  var self = this;

  // impressions is optional
  if (typeof impressions === 'function') {
    callback = impressions;
    impressions = 1;
  }

  // get a key for today
  var key = moment().format('YYYYMMDD');

  if (this.impressions_by_date == undefined)
    this.impressions_by_date = {};

  if (this.impressions_by_date[key] === undefined) {
    this.impressions_by_date[key] = impressions;
  } else {
    this.impressions_by_date[key]+= impressions;
  }

  // save the most recent click date
  this.impressions_last = Date.now();
  this.impressions_count += impressions;

  // make it for saving
  this.markModified('impressions_by_date'); // SO LAME

  // save that shit
  this.save( function(err) {
    if (!!err)
      return callback(err);
    else
      return callback(null);
  });
});

// incr click
AdvertSchema.method('add_click', function(callback) {
  var self = this;

  // get a key for today
  var key = moment().format('YYYYMMDD');

  if (this.clicks_by_date == undefined)
    this.clicks_by_date = {};

  if (this.impressions_by_date[key] === undefined) {
    this.clicks_by_date[key] = 1;
  } else {
    this.clicks_by_date[key]+= 1;
  }

  // save the most recent click date
  this.clicks_last = Date.now();
  this.clicks_count += 1;

  // make it for saving
  this.markModified('clicks_by_date'); // SO LAME

  // save that shit
  this.save(function(err) {
    if (err)
      throw err;

    callback();
  });

});


module.exports = mongoose.model('Advert', AdvertSchema);
