var mongoose = require('mongoose');
var bcrypt = require('bcryptjs')
  , roles = require('../helpers/roles')
  , utilities = require('mongoose-utilities')
  , presave = require('../helpers/presave');

// Schema data
var UserSchema = new mongoose.Schema({
  name: {
    type: String,

    required: true,
    trim: true
  },

  email: {
    type: String,
    required: true,
    lowercase: true,
    trim: true,

    match: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA -Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    // stupid vim syntax is broken, fix -> "

    index: {
      unique: true,
      dropDups: true
    }
  },

  phone: {
    type: String,
    required: false,
    trim: true,

    match: /^(\d{8,12})?$/
  },

  password: {
    type: String,
    required: true
  },

  token: String,

  // Payments.
  client_id: String,

  // there are a few problems with this
  // however it also makes logic much easier...
  subscriptions: [{
    magazine: String,
    subscription_id: String,

    start_date: Date,
    end_date: Date
  }],

  authorisation: {
    type: Number,
    default: 0
  },

  cv: {
    type: String,
    trim: true
  },

//  vacancy: [{
//    type: String,
//    trim: true
//  }],

  created_at: Date,
  modified_at: Date
});

// pagination
UserSchema.plugin(utilities.pagination);

UserSchema.pre('save', function(next) {
  var user = this;
  var SALT_WORK_FACTOR = 10;

  // only hash the password if it has been modified (or is new)
  if(user.isModified('password')) {
    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
      if (err)
        return next(err);

      // hash the password along with our new salt
      bcrypt.hash(user.password, salt, function(err, hash) {
        if (err)
          return next(err);

        console.log('[hash] %s -> %s', user.password, hash);

        // override the cleartext password with the hashed one
        user.password = hash;

        // do the next stuff
        presave.update(next);
      });
    });
  }

  if( !user.client_id ) {
    var Paymill = require('../helpers/paymill').client;

    console.log(user.email);
    Paymill.clients.create( { email: user.email }, function(err,client) {
      if (err) {
        console.log("Couldn't create the customer record");
        return;
      }

      user.client_id = client.data.id;
      presave.update(next);
    });
  } else {
    presave.update(next);
  }
});

// used in the templates to grab a nice url
UserSchema.method('edit_path', function() {
  return '/admin/users/' + this.id + '/edit'
});

// Create a user account
UserSchema.static('createUser', function(user) {
  var self = this;
  User.create(user);
});


// Check if given password matches user password
UserSchema.method('isPassword', function(password, callback) {
  var self = this;

  if (password == undefined) {
    callback(false, false);
  } else {
    bcrypt.compare(password, self.password, function(err, same) {
      callback(err, same);
    });
  }
});

// NewsSchema.method('requiresRole', function() { })
UserSchema.method('hasAuth', function(role) {
  var level = roles.checkRole(role);
  if( level === false ) throw "Invalid argument 1 of 1";

  return level <= this.authorisation;
});

UserSchema.method('setAuth', function(role) {
  var level = roles.checkRole(role);
  if( level === false ) throw "Invalid argument 1 of 1";

  this.authorisation = level;
});
// UserSchema.method('addAuth', function(role) {});
// UserSchema.method('removeAuth', function(role) {});
// UserSchema.method('removeAuth', function(role) {});

UserSchema.method('canAccess', function(item) {
  // try { item.requiresRole(); } catch(ReferenceError) { }
  // doesn't exist (not news) or < role (news 0/1, users 4)
  return !item.restricted ||  this.hasAuth( (item.restricted) ? 1 : 0 );
});

UserSchema.method('hasSubscription', function(item) {
  //Check if user has subscription to selected item,
  if (!this.subscriptions || this.subscriptions.length == 0) {
    return false;
  }
  for (var i = 0, len = this.subscriptions.length; i < len; i++) {
    if (this.subscriptions[i].magazine == item && this.subscriptions[i].end_date >= Date.now()) {
      return true;
    }
  }
  return false;
});

UserSchema.method('getSubscriptions', function() {
  var subs = [];
  if (!this.subscriptions) {
    return [];
  }
  for (var i = 0, len = this.subscriptions.length; i < len; i++) {
    if (this.subscriptions[i].end_date >= new Date()) {
      subs.push(this.subscriptions[i].magazine);
    }
  }
  return subs;
});

module.exports = mongoose.model('User', UserSchema);
