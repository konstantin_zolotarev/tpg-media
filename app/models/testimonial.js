var mongoose = require('mongoose')
    , utilities = require('mongoose-utilities');

var TestimonialSchema = new mongoose.Schema({
    title: String,
    image: String,

    created_at: Date,
    modified_at: Date
});


// load paginate plugin
TestimonialSchema.plugin(utilities.pagination);

//set indexes
TestimonialSchema.index({
    title: 'text',
    image: 'text'
});

var presave = require('../helpers/presave');

TestimonialSchema.pre('save', function(next) {
    // presave helper
    presave.update(next);
});

module.exports = mongoose.model('Testimonial', TestimonialSchema);