var mongoose = require('mongoose')
  , textSearch = require('mongoose-text-search')
  , markdown = require( "markdown" ).markdown;


var IssueSchema = new mongoose.Schema({
  title: String,
  link: String,
  image: String,

  magazines: [String],

  created_at: Date,
  modified_at: Date,
  published_at: Date
});

// load textSearch plugin
IssueSchema.plugin(textSearch);

// set index
IssueSchema.index({
  title: 'text',
  link: 'text',
  image: 'text'
});

// set appropriate created/edited dates
var presave = require('../helpers/presave');

IssueSchema.pre( 'save', function(next) {
  var issue = this;

  // if published state has changed update the published_at date
  if (issue.isModified('published'))
    issue.published_at = new Date();

  // presave helper
  presave.update(next)
});


// set boolean of restricted
IssueSchema.method('restrict', function(bool, callback) {
  if( typeof(bool) != 'boolean' )
    throw 'Non-boolean argument given';

  this.restricted = bool;

  return callback(bool, this);
});


module.exports = mongoose.model('Issue', IssueSchema);
