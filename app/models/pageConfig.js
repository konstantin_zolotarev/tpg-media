var mongoose = require('mongoose');

var PageConfigSchema = new mongoose.Schema({
  title: String,
  page: String,
  image: String,

  created_at: Date,
  modified_at: Date
});

// Indexes
PageConfigSchema.index({
  page: 'text',
  image: 'text'
});

var presave = require('../helpers/presave');

PageConfigSchema.pre('save', function(next) {
  // presave helper
  presave.update(next);
});

module.exports = mongoose.model('PageConfig', PageConfigSchema);