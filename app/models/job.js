var mongoose = require('mongoose');

var JobSchema = new mongoose.Schema({
  title: String,
  description: String,
  email: String,
  website: String,

  created_at: Date,
  modified_at: Date
});

// set index
JobSchema.index({
  title: 1,
  email: 1
});

// set appropriate created/edited dates
var presave = require('../helpers/presave');

JobSchema.pre( 'save', function(next) {
  if (!this.created_at)
    this.created_at = new Date();
  // presave helper
  presave.update(next)
});

JobSchema.method('getWebsite', function() {
  if (this.website.indexOf('http') >= 0 || this.website.indexOf('//') >= 0 || this.website.indexOf('/') == 0) {
    return this.website;
  }
  return 'http://'+this.website;
});

module.exports = mongoose.model('Job', JobSchema);
