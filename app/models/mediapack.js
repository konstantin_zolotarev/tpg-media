var mongoose = require('mongoose');
var utilities = require('mongoose-utilities');

// Schema data
var MediaPackSchema = new mongoose.Schema({
  title: String,
  image: String,
  description: String,
  link: String,

  created_at: Date,
  modified_at: Date
});

// load paginate plugin
MediaPackSchema.plugin(utilities.pagination);

var presave = require('../helpers/presave');
MediaPackSchema.pre( 'save', function(next) { presave.update(next) } );

module.exports = mongoose.model('MediaPack', MediaPackSchema);
