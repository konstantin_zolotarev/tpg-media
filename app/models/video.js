var mongoose = require('mongoose')
  , markdown = require( "markdown" ).markdown;

var VideoSchema = new mongoose.Schema({
  title: String,
  url: String,

  tags: [String],
  magazines: [String],

  created_at: Date,
  modified_at: Date
});

// load textSearch plugin
// VideoSchema.plugin(textSearch);

// set index
VideoSchema.index({
  title: 'text',
  tags: 'text'
});

// set appropriate created/edited dates
var presave = require('../helpers/presave');

VideoSchema.pre( 'save', function(next) {
  var Video = this;

  // if published state has changed update the published_at date
  if (Video.isModified('published'))
    Video.published_at = new Date();

  // presave helper
  presave.update(next)
});


// set boolean of restricted
VideoSchema.method('restrict', function(bool, callback) {
  if( typeof(bool) != 'boolean' )
    throw 'Non-boolean argument given';

  this.restricted = bool;

  return callback(bool, this);
});




module.exports = mongoose.model('Video', VideoSchema);
