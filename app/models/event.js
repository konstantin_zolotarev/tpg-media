var mongoose = require('mongoose')
  , textSearch = require('mongoose-text-search');

var EventSchema = new mongoose.Schema({
  title: String,

  date: Date,
  location: String,
  city: String,
  country: String,
  website: String,
  event_type: String,

  magazines: [String],

  created_at: Date,
  modified_at: Date
});

// load textSearch plugin
EventSchema.plugin(textSearch);

// set index
EventSchema.index({
  title: 'text'
});

// set appropriate created/edited dates
var presave = require('../helpers/presave');

EventSchema.pre( 'save', function(next) {
  this.modified_at = new Date();
  // presave helper
  presave.update(next)
});

EventSchema.method('getWebsite', function() {
  if (this.website.indexOf('http') >= 0 || this.website.indexOf('//') >= 0 || this.website.indexOf('/') == 0) {
    return this.website;
  }
  return 'http://'+this.website;
});

module.exports = mongoose.model('Event', EventSchema);
