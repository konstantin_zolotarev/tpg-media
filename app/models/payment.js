var mongoose = require('mongoose');

var PaymentSchema = new mongoose.Schema({
  subscription: String,
  amount: Float,
  status: Boolean,
  payment_date: Date
});