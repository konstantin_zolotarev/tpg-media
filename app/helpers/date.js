Date.prototype.getOrdinal = function() {
  var date = this.getDate();
  return (date > 20 || date < 10) ? ([false, "st", "nd", "rd"])[(date%10)] || "th" : "th";
}

Date.prototype.pretify = function() {
  return this.getDate() +' '+ this.getMonthName() +' '+ this.getFullYear();
}

Date.prototype.forInput = function() {
    var str = this.getFullYear();
    if (this.getMonth() < 9) {
        str += '-0'+(this.getMonth()+1);
    } else {
        str += '-'+(this.getMonth() + 1);
    }
    if (this.getDate() < 10) {
        str += '-0'+(this.getDate());
    } else {
        str += '-'+(this.getDate());
    }
    return str;
}

Date.prototype.getMonthName = function() {
  var months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];

  return months[this.getMonth()];
}

Date.prototype.getMonthShortName = function() {
  return this.getMonthName().substr(0,3);
}

Date.prototype.today = function() {
  var today = new Date();
  return today.getDate() +"/"+ (today.getMonth()+1) +"/"+ today.getFullYear()
}

