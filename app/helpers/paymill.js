var config = require('../../config/app').paymill

module.exports = {
  private_key: config.test.private,
  public_key: config.test.public,
  client: require('paymill-node')(config.test.private)
}

// var Paymill = require('paymill-node')('9443755258e6ee388cffa72ac478837b');
