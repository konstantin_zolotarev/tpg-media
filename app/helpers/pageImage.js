var PageConfig = require('../models/pageConfig');

/**
 * Load image for page
 *
 * @param {string} page
 * @param {function} cb
 */
module.exports = function(page, cb) {
  PageConfig.findOne({
    page: page
  }, cb);
};