var PreSave = function() {};

PreSave.prototype.update = function(next) {
  if( !this.created_at )
    this.created_at = Date.now();

  this.modified_at = Date.now();

  next();
};

// =-=-=-=-=-=-=-=-=-=-=-=-=-=-= //
module.exports = new PreSave;

