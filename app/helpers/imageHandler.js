var imagemagick = require('imagemagick')
  , aws = require('../../config/app').aws
  , knox = require('knox');

var ImageHandler = function() {
  this.client = knox.createClient({
    key: aws.accessKey,
    secret: aws.secretKey,

    bucket: aws.bucket
  });

  this.isPublic = true;
  this.defaultHeight = 190;
  this.defaultWidth = 400;
}

ImageHandler.prototype.targetDir = function(path) {
  this.targetDirectory = path;
}

ImageHandler.prototype.resizeFile = function( opts, callback) {
    return imagemagick.crop({
      srcPath: opts.src,
      dstPath: opts.dest,
      width: opts.width,
      height: opts.height//,
      // quality: 0.1,
      // format: 'jpg',
      // progressive: false,
      // strip: true,
      // sharpening: 0.2
    }, function(err, stdout, stderr ) {
      callback( opts, { err: err, stdout: stdout, stderr: stderr } );
    });
}


ImageHandler.prototype.getData = function( path, callback ) {
  imagemagick.identify(_file.path, function(err, metadata) {
    this.metadata = metadata;
    return callback(err,metadata);
  });
}

ImageHandler.prototype.countOb = function(ob) {
  var size;
  for( key in ob ) { if( ob.hasOwnProperty(key) ) size++; }

    return size;
};

ImageHandler.prototype.prepare = function( file, sizes, callback ) {
  var self = this;

  if( !sizes ) return false;
  var size = (typeof(sizes) !== 'array' ) ? [sizes] : sizes;

  var output = [];
  sizes.forEach( function(size) {
    self.resizeFile({
      src: file.path,
      dest: file.path + (size.retina === true ? "@2x" : ""),
      width: size.width,
      height: size.height
    }, function(opts, response) {
      if(response.err) throw response.err;
      if(response.stderr) throw response.stderr;

      var filetype = file.name.slice( file.name.lastIndexOf('.') );
      var filename = file.name.slice( 0, file.name.lastIndexOf('.') ) + (size.retina === true ? "@2x" : "") + filetype;

      output.push( { 'name': filename, 'path': opts.dest } );
      if( output.length == sizes.length ) callback(output);
    })
  });
}

ImageHandler.prototype.uploadFiles = function( data, apply ) {
  var self = this
  , responses = [];

  return data.forEach( function(e) {1
    self.uploadFile( e.file.path, e.path + Date.now() + '_' + e.file.name, function(res) {

      responses.push(res.client._httpMessage.url);
      if( data.length == responses.length ) {
        apply(responses);
      }
    } )
  });
}

ImageHandler.prototype.uploadFile = function( file, path, callback ) {
  var isPublic = (this.isPublic === false) ? {} : { 'x-amz-acl': 'public-read' };
  var Client = this.client;

  Client.putFile( file, path, isPublic, function(err, res) {
    if( err ) throw err;
    return callback(res);
  });
}

ImageHandler.prototype.deleteImage = function(file, path, callback) {
  this.client.deleteFile();
};

module.exports = new ImageHandler;
