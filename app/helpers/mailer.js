var config      = require('../../config/app');
var jade        = require('jade');
var nodemailer  = require('nodemailer');
var util        = require('util');

// constructor
var mailer = function() {
  var self = this;

  this.transport = nodemailer.createTransport('SES', {
    AWSAccessKeyID: config.aws_mailer.accessKey,
    AWSSecretKey: config.aws_mailer.secretKey,
    ServiceUrl: config.aws_mailer.serviceUrl
  });
};

mailer.prototype.format = function(name, email) {
   return name + '<' + email + '>';
};

mailer.prototype.send = function(template, options, data) {
  var self = this;

  // render templates
  jade.renderFile('app/views/mail/' + template + '.text.jade', data, function (err, text) {
    if (err)
      throw err;

    jade.renderFile('app/views/mail/' + template + '.html.jade', data, function (err, html) {
      if (err)
        throw err;

      // save messages
      options.text = text;
      options.html = html;

      // send mail
      self.transport.sendMail(options, function(err, res) {
        if(err) 
          throw err;

        // Er... Do something...
      });
    });
  });
}

mailer.prototype.userMail = function( subject, template, user, host ) {
  var self = this;
  var preSubject = settings.magazine.charAt(0).toUpperCase() + settings.magazine.slice(1) + ' magazine: ';

  this.send(template, { 
    from: config.mail.sender.name + '<' + config.mail.sender.address + '>',
    to: user.name + ' <' + user.email + '>',
    subject: preSubject + subject
  }, { 
    user: user,
    host: host
  });
}

// send newsletter subscriber notification
mailer.prototype.newsLetter = function(template, email ) {
  var self = this;
  var preSubject = settings.magazine.charAt(0).toUpperCase() + settings.magazine.slice(1) + ' magazine: ';

  this.send(template, {
    from: config.mail.sender.name + '<' + config.mail.sender.address + '>',
    to: config.mail.newsletter.name + ' <' + config.mail.newsletter.address + '>',
    cc: config.mail.newsletter.cc,
    subject: preSubject + config.mail.newsletter.subject
  }, {
    email: email
  });
}

module.exports = new mailer();
