// This is the firewall
// We will use locomotive filters to protect certain parts of the site.
//
// http://locomotivejs.org/guide/filters/

// A quick explination
// -------------------
//
// all firewall levels require the user to be authenticated.
// 'none' does not mean no priviledges or no user, it means a user with no priviledges
// 'read' means subscribed
// 'write' means can write articles
// 'publish' means can publish articles
// 'admin' means full access
//
// A route with no protection should NOT use the firewall.

var firewall = {
  none: function(req, res, next) {
    // redirect if user is not authenticated
    if (!req.isAuthenticated()) {
      res.redirect('/account/login?path=' + req.path);
    } else {
      next();
    }
  },

  read: function(req, res, next) {
    // redirect if user is not authenticated
    if (!req.isAuthenticated()) {
      res.redirect('/account/login?path=' + req.path);
    } else {
      // check has permission
      if (!req.user.hasAuth('read')) {
        res.send('You do not have read permission.');
      } else {
        next();
      }
    }
  },

  subscribed: function(req, res, next) {
    // redirect if user is not authenticated
    if (!req.isAuthenticated()){
      res.redirect('/account/login?path=' + req.path);
    } else {

      // if we have write then we need to view everything anyway
      if (!req.user.hasAuth('write')) {
        // important variables
        var magazine = this.__app.get('magazine');    
        var subscriptions = req.user.subscriptions;
        
        // see if ones of these subscriptions is valid
        var subscribed = false;
        var today = new Date();

        // check all the subscriptions
        subscriptions.forEach(function(subscription) {
          if (subscription.magazine == magazine)
            if (subscription.start_date < today && today < subscription.end_date )
              subscribed = true;
        });

        // do the final bit
        if (!subscribed) {
          res.redirect('/subscribe');
        } else {
          next();
        }
      } else {
        // we have write or more
        next();
      }
    }
  },


  write: function(req, res, next) {
    // redirect if user is not authenticated
    if (!req.isAuthenticated()) {
      res.redirect('/account/login?path=' + req.path);
    } else {
      // check has permission
      if (!req.user.hasAuth('write')) {
        res.send('You do not have write permission.');
      } else {
        next();
      }
    }
  },

  publish: function(req, res, next) {
    // redirect if user is not authenticated
    if (!req.isAuthenticated()) {
      res.redirect('/account/login?path=' + req.path);
    } else {
      // check has permission
      if (!req.user.hasAuth('publish')) {
        res.send('You do not have publish permission.');
      } else {
        next();
      }
    }
  },

  admin: function(req, res, next) {
    // redirect if user is not authenticated
    if (!req.isAuthenticated()) {
      res.redirect('/account/login?path=' + req.path);
    } else {
      // check has permission
      if (!req.user.hasAuth('admin')) {
        res.send('You do not have admin permission.');
      } else {
        next();
      }
    }
  },
};

module.exports = firewall;
