/*
  0 : 'none',
  1 : 'execute',
  2 : 'write',
  3 : 'write & execute',
  4 : 'read',
  5 : 'read & execute',
  6 : 'read & write',
  7 : 'full'
*/

var Roles = function() {
  // why ever would you need constants?
  this.roles = {
    0: 'none',
    1: 'read',
    2: 'write',
    3: 'publish',
    4: 'admin'
  }
}

Roles.prototype.getKey = function( value ) {
  for( var prop in this.roles ) {
    if( this.roles[ prop ] === value ) return prop;
  }
}

Roles.prototype.checkRole = function(role) {
  switch( typeof(role) ) {
    case "number" :
      if( role > 4 || role < 0 ) return false;
      return (role % 1 === 0) ? role : false;

      break; // Failsafe?
    case "string" :
      return this.getKey(role);

      break;
    case undefined :
    default :
      return false;
  }

  // return ( !role || typeof(role) != 'number' ||  ) ? false : true;
}

Roles.prototype.toString = function(int) {
  return this.roles[int];
}

Roles.prototype.toInt = function(str) {
  return this.getKey(str);
}


// =-=-=-=-=-=-=-=-=-=-=-=-=-=-= //
module.exports = new Roles;
