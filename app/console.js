var lcm   = require('jake-lcm')
  , repl  = require('repl');

// start lcm environment
lcm.exec({
  initializers: ['00_generic', '02_mongoose', '03_models', '04_passport']
}, function () {
  // start repl
  repl.start({
    prompt: "Lcm: > ",
    input: process.stdin,
    output: process.stdout
  });
});
