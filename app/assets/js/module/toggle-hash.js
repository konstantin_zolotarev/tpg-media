(function($){
  /*
    clear hash when link is subsequently clicked
  */
  $('a[href^="#"]').on('click',function(e){
    e.preventDefault();
    var hash = window.location.hash,
    href = $(this).attr('href'),
    scroll_to = $(this).data('scroll-to');

    //link href is same as hash?
    if(hash === href){
      //clear the hash!
      window.location = "#";
    } else {
      //set the hash
      window.location.hash = href;
      if( typeof scroll_to === 'number' ){
        window.scroll( 0, parseInt(scroll_to) );
      }
    }
  });
})(jQuery);