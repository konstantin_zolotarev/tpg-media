if( $('#editor_code').length ){
  $('#editor_code')
    .on('focus',function(e){
      $('#editor_container').addClass('focussed');
    })
    .on('blur',function(){
      $('#editor_container').removeClass('focussed');
    });
}
