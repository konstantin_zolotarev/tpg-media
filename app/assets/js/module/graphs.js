(function() {
  function lineGraph(element, src, label, w, h) {
    var graph  = $(element).children('div.display')[0];

    var src    = $(graph).data('src');
    var label  = $(graph).data('label');
    var width  = $(graph).data('width')  || 960;
    var height = $(graph).data('height') || 500;

    var start  = $(element).children('input[name=start]').val();
    var stop   = $(element).children('input[name=stop]').val();

    var margin = { top: 20, right: 20, bottom: 30, left: 50 };
    width = width - margin.left - margin.right;
    height = height - margin.top - margin.bottom;

    var parseDate = d3.time.format('%Y%m%d').parse;

    var x = d3.time.scale().range([0, width]);
    var y = d3.scale.linear().range([height, 0]);

    var xAxis = d3.svg.axis().scale(x).orient('bottom');
    var yAxis = d3.svg.axis().scale(y).orient('left');

    var line = d3.svg.line()
      .interpolate('basis')
      .x(function(d) { return x(d.date); })
      .y(function(d) { return y(d.value); });

    var svg = d3.select(graph).append('svg')
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .append('g')
      .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

    d3.csv(src + '/' + start + '/' + stop, function(error, data) {
      data.forEach(function(d) {
        d.date = parseDate(d.date);
        d.value = parseFloat(d.value);
      });

      x.domain(d3.extent(data, function(d) { return d.date; }));
      y.domain(d3.extent(data, function(d) { return d.value; }));

      svg.append('g')
        .attr('class', 'x axis')
        .attr('transform', 'translate(0,' + height + ')')
        .call(xAxis);

      svg.append('g')
        .attr('class', 'y axis')
        .call(yAxis)
        .append('text')
        .attr('transform', 'rotate(-90)')
        .attr('y', 6)
        .attr('dy', '.71em')
        .style('text-anchor', 'end')
        .text(label);

      svg.append('path')
        .datum(data)
        .attr('class', 'line')
        .attr('d', line);
    });
  }

  $('[data-graph]').each(function() {
    var graph = this;

    lineGraph(this);

    // hook into inputs and redraw on change
    $(this).children('input').change(function(){
      $(graph).children('.display').empty();
      lineGraph(graph);
    });
  });
})();
