(function($){
  var nav = $('#main-nav');
  var nav_content = nav.clone();
  nav.remove();

  $('#nav-toggle').after( nav_content );
})(jQuery);