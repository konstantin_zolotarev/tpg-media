Feedback =
  "form" :
    "card-paymentname": "Creditcard"
    "card-number": "Cardnumber"
    "card-cvc": "CVC"
    "card-holdername": "Cardholder"
    "card-expiry": "Valid until (MM/YYYY)"
    "amount": "Amount"
    "currency": "Currency"
    "submit-button": "Submit"
    "elv-paymentname": "Directdebit"
    "elv-account": "Accountnumber"
    "elv-holdername": "Accountholder"
    "elv-bankcode": "Bankcode"
  "error" :
    "invalid-card-number": "Invalid cardnumber."
    "invalid-card-expiry-date": "Invalid expire-date."
    "invalid-card-holdername": "Please enter the cardholders name."
    "invalid-elv-holdername": "Please enter the accountholders name."
    "invalid-elv-accountnumber": "Please enter a valid accountnumber."
    "invalid-elv-bankcode": "Please enter a valid bankcode."

PaymillResponseHandler = (error, result) ->
  if error
    $(".payment-errors").text error.apierror
    $(".submit-button").removeAttr "disabled"
  else
    # alert result.token
    form = $("#payment-form")
    form.append $("<input>", { type: 'hidden', name: 'paymillToken', value: result.token })
    form.get(0).submit()

$("#payment-form").on 'submit', (event) ->
  event.preventDefault
  $(".submit-button").attr "disabled", "disabled"

  paymenttype = (if ($(".paymenttype.disabled").length > 0) then $(".paymenttype.disabled").val() else "cc")

  switch paymenttype
    when "cc"
      params =
        number:     $(".card-number").val()
        exp_month:  $(".card-expiry-month").val()
        exp_year:   $(".card-expiry-year").val()
        cvc:        $(".card-cvc").val()
        cardholder: $(".card-holdername").val()

      if paymill.validateCardNumber( params.number ) is false
        $(".payment-errors").text Feedback.form["invalid-card-number"]
        $(".payment-errors").css "display", "inline-block"
        $(".submit-button").removeAttr "disabled"
        return false
      if paymill.validateExpiry( params.exp_month, params.exp_year ) is false
        $(".payment-errors").text Feedback.form["invalid-card-expiry-date"]
        $(".payment-errors").css "display", "inline-block"
        $(".submit-button").removeAttr "disabled"
        return false
      if params.cardholder is false or params.cardholder is ""
        $(".payment-errors").text Feedback.form["invalid-card-holdername"]
        $(".payment-errors").css "display", "inline-block"
        $(".submit-button").removeAttr "disabled"
        return false

    when "elv"
      params =
        number: $(".elv-account").val()
        bank: $(".elv-bankcode").val()
        accountholder: $(".elv-holdername").val()

      if params.accountholder is false or params.accountholder is ""
        $(".payment-errors").text Feedback.error["invalid-elv-holdername"]
        $(".payment-errors").css "display", "inline-block"
        $(".submit-button").removeAttr "disabled"
        return false
      if paymill.validateAccountNumber( params.number ) is false
        $(".payment-errors").text Feedback.error["invalid-elv-accountnumber"]
        $(".payment-errors").css "display", "inline-block"
        $(".submit-button").removeAttr "disabled"
        return false
      if paymill.validateBankCode( params.bank ) is false
        $(".payment-errors").text Feedback.error["invalid-elv-bankcode"]
        $(".payment-errors").css "display", "inline-block"
        $(".submit-button").removeAttr "disabled"
        return false

  paymill.createToken params, PaymillResponseHandler
  false
