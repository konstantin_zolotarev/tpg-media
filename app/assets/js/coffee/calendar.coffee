class Calendar
  constructor: ->
    @dD = new Date()
    @current_day = @dD.getDate()
    @current_month = @dD.getMonth()  + 1
    @current_year = @dD.getFullYear()

  monthAsString: ->
    switch @current_month
      when 1 then "January"
      when 2 then "February"
      when 3 then "March"
      when 4 then "April"
      when 5 then "May"
      when 6 then "June"
      when 7 then "July"
      when 8 then "August"
      when 9 then "September"
      when 10 then "October"
      when 11 then "November"
      when 12 then "December"

  shortMonthAsString: ->
    switch @current_month
      when 1  then "jan"
      when 2  then "feb"
      when 3  then "mar"
      when 4  then "apr"
      when 5  then "may"
      when 7  then "jul"
      when 8  then "aug"
      when 9  then "sep"
      when 10 then "oct"
      when 11 then "nov"
      when 12 then "dec"

  todayAsString: ->
    switch @Dd.getDay()
      when 0 then "Sunday"
      when 1 then "Monday"
      when 2 then "Tuesday"
      when 3 then "Wednesday"
      when 4 then "Thursday"
      when 5 then "Friday"
      when 6 then "Saturday"

  firstDayOfMonth: ->
    first = new Date("#{@current_month}/1/#{@current_year}")

cal = new Calendar
$('section.calendar img').addClass cal.shortMonthAsString()
