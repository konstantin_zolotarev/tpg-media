ad_container = document.querySelectorAll('.ads .top, .ads .bottom')
#vertical advert placeholder
ad_container_horizontal = document.querySelectorAll('.vads .vtop, .vads .vbottom')

injectImages = ->
  return false if ad_container is null

  getContainer = (containers, order) ->
    return false if containers.length is 0
    if ~order and containers[ order ]
      return containers[ order ]
    else
      return containers[ Math.floor(Math.random() * containers.length) ]

  inject = (layoutCol, element, parent) ->
    return false if element is null

#    if parent is undefined then containers = ad_container
#    else containers = document.querySelectorAll(parent)

    if element.type is 'vertical'
      contain = getContainer ad_container, layoutCol
    else
      return false if ad_container_horizontal.length is 0
      contain = getContainer ad_container_horizontal
      return false if contain.querySelector 'figure'

    return false if not contain

    el = document.createElement("figure")
    anchor = document.createElement("a")
    anchor.href = "/adverts/" + element._id
    anchor.target = "_blank"

    return false if element.images.length is 0 or element.images[0] == ''

    image = new Image
    image.id = element._id
    image.src = element.images[0];
    image.onload = ->
      this.className = "ad_loaded"



    #Clear placeholders
    placeholder = contain.querySelectorAll('.placeholder')
    for pl in placeholder
      contain.removeChild pl

    contain.appendChild(el).appendChild(anchor).appendChild(image)

  ajaxLoad "/adverts", (xhr) ->
    json = JSON.parse xhr.response

    for sub, arr of json
      arrLength = arr.length
      if sub is "small"
#        inject arrLength, el for el in arr if arrLength

        if arrLength
          elProgress = 0
          arrLengthHalf = arrLength / 2
          for el in arr
            if elProgress > arrLengthHalf
              inject 1, el # 1 means right project column
            else
              inject 0, el # 0 means left project column
            elProgress++

      if sub is "featured"
        inject arrLength, el for el in arr if arrLength
      if sub is "horizontal"
        inject arrLength, el for el in arr if arrLength


window.onload = ->
  setTimeout injectImages, 5

elementPosition = (el) ->
  posY = 0
  while el and not isNaN(el.offsetTop)
    posY += el.offsetTop # - el.scrollTop
    el = el.offsetParent
  posY

trackAdvert = (advert) ->
  advert.className = ""
  ajaxLoad "/capture/adverts/#{advert.id}", (xhr) ->
    json = JSON.parse xhr.response

scrollEvent = (bottom) ->
  # top : pageYOffset
  adverts = []
  for container in ad_container
    adverts.push container.querySelectorAll '.ad_loaded'

  return false if adverts.length < 1

  for i,ad of adverts
    position = elementPosition ad
    trackAdvert ad if pageYOffset < position < bottom and position > 0

if ad_container isnt null
  window.onscroll = ->
    # height = window.innerHeight
    # docheight = document.getElementsByTagName('body')[0].clientHeight
    # offset = pageYOffset
    bottom = window.innerHeight + pageYOffset
    scrollEvent(bottom)

  # Init.
  scrollEvent( window.innerHeight + pageYOffset )
