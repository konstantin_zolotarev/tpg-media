root = exports ? this
root.ajaxLoad = (url, callback) ->
  xhr = undefined

  ensureReadiness = ->
    return if xhr.readyState < 4 or xhr.status  isnt 200
    callback xhr if xhr.readyState is 4

  unless typeof XMLHttpRequest is 'undefined' then xhr = new XMLHttpRequest()
  else
    for version in ["MSXML2.XmlHttp.5.0", "MSXML2.XmlHttp.4.0", "MSXML2.XmlHttp.3.0", "MSXML2.XmlHttp.2.0", "Microsoft.XmlHttp"]
      try xhr = new ActiveXObject(version)

  xhr.onreadystatechange = ensureReadiness
  xhr.open "GET", url, true
  xhr.send null
