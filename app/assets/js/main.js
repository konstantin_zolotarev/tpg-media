//= require module/webfonts.js

//= require vendor/jquery.js
//= require vendor/domplus.js
//= require vendor/marked.js
//= require vendor/d3.js

//= require module/markdown.js
//= require module/markdown-nicities.js
//= require module/responsive-nav.js
//= require module/toggle-hash.js
//= require module/graphs.js
//= require module/isotope.js

//= require coffee/ajax.coffee
//= require coffee/impression.coffee
//= require coffee/paymill.coffee
//= require coffee/news.coffee
//= require coffee/calendar.coffee

//= require theme/theme-scripts.js
//= require theme/lightbox.js

(function(){
  if( document.getElementById('editor') ) {
    MarkdownEditor.init();
  }
})();

var input = document.getElementById('filesToUpload');

$(function() {
  $('input[type=file].autoupload').on('change', function() {
    var self = $(this);
    self.attr('disabled', 'disabled');
    $('p.error').remove();

    // for( var x = 0; x < input.files.length; x++ ) {
    for (var i = input.files.length - 1; i >= 0; i--) {
      var _this = input.files[i];
      var _form = new FormData();

      _form.append("file", _this);
      _form.append("path", $(input).data('destination') );
      _form.append("type", $('select[name="type"]').val() );
      if( $('form#ad [name=featured]')[0] ) {
        _form.append("featured", $('form#ad [name=featured]')[0].checked );
      } else {
        _form.append("featured", null );
      }

      // var parent = self.data('minwidth')
      // if( parent ) { _form.append( "minwidth", parent ) }

      $.ajax({
        url: "/upload/"+ self.data('destination'),
        type: 'post',
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        data: _form,
        success: function(res, data, jqxhr) {

          var cur = $('textarea#editor_code').text();
          var str =  cur + "\n![<<TODO: Describe this image.>>](" + res.url + ")";
          $('textarea#editor_code').text(  str  );

          if (cur.length == 0) {
            if( $('input[name="header"]').length > 0 ) {
              $('input[name="header"]').val( res.url );
              //            $('input[type=file]').after( $('<img>', { src: res.url }) );
            }
          }

          if( $('input#image_url').length > 0 ) {
            $('input#image_url').val( res.url );
  //            $('input[type=file]').after( $('<img>', { src: res.url }) );
          }

          var previewArea = $('.preview-images', self.parent());
          if (previewArea) {
            if (res.url instanceof Array) {
              for(var i = 0; i < res.url.length; i++) {
                previewArea.append( $('<img>', { src: res.url[i] }) );
              }
            } else {
              previewArea.append( $('<img>', { src: res.url }) );
            }
          }

          self.data('icon', 's');
        }
      }).done( function(response) {
        if(response.error) {
          var elem = $('<p>', { class: 'error'} ).text(response.error);
          self.before( elem );
        }

        self.attr('disabled', false);
//        self.val('');
      });
    }

  });

  // delete hook
  $('a[data-ajax]').on('click', function(event) {
    event.preventDefault();

    // silly antipattern
    var self = this;

    // get the data we need
    var type = $(this).attr('data-ajax');
    var url = $(this).attr('href');

    // confirmation dialog
    var destroy = confirm('Delete?');

    // send the request
    var sendXHR = function() {
      $.ajax({
        type: type,
        url: url,

        success: function(data) {
          // remove element
          $(self).parents('tr').hide('fast');
        },

        error: function() {
          // handle an error
          alert('An error has occured');
        }
      });
    };

    if (destroy)
      sendXHR();
  });

  // ajax form
  $('form[data-ajax]').on('submit', function(event) {
    event.preventDefault();

    // save this!
    var form = $(this);

    // ajax request
    var action = $(this).attr('action');
    var method = $(this).attr('method');

    // payload
    var payload = {};

    // populate payload
    $(this).find('[name]').each(function(index) {
      payload[$(this).attr('name')] = $(this).val();
    });

    // send request
    var request = $.ajax({
      type: method,
      url: action,

      data: payload
    });

    // success hook
    request.done(function(data) {
      // clear the form
      $(form)[0].reset();

      // remove old messages, prepend success element
      $('.success,.failure').remove();
      $(form).before('<p class=success>Thanks, your message has been sent.</p>');
    });

    // fail hook
    request.fail(function(xhr, status) {
      // remove old messages, prepend failure element
      $('.success,.failure').remove();
      $(form).before('<p class=failure>Oops, an error has occured.</p>');
    });

    console.log(payload);
  });

  function getLocation()  {
    if (navigator.geolocation)
    {
      navigator.geolocation.getCurrentPosition(getWeather, getWeather);
    } else {
      getWeather();
    }
  }

  function getWeather(pos) {

    var position = {};

    if (pos && !pos.code) {
      position = {
        lat: pos.coords.latitude,
        long: pos.coords.longitude
      }
    }

    $.get("/weather", position, function(data) {
      $('#weather').html(data);
    }, 'html');

  }

  //coomented as weather block had been commented earlier
//  getLocation();
});

// remove account cv file
$(document).ready(function(){
  $('#deletecv').click(function(){
    $.ajax({
      url: '/account/deletecv/' + $('#userId').val(),
      async: true,
      type: 'GET',
      success: function(result){
        $('#currentCV').text('');
      }
    });
  });

  // disable subscribe button if email field empty
  $('#email-newsletter').keyup(function() {
    if($(this).val() != '') {
      $('input[type="submit"]').attr('disabled','disabled')
    } else {
      $('input[type="submit"]').removeAttr('disabled');
    }
  });

  // subscribe me for newsletter
  $('#newsletter-subscribe').on('submit', function(event){
    event.preventDefault();

    $('input[type="submit"]').attr('disabled','disabled');


    // ajax request
    var action = $(this).attr('action');
    var method = $(this).attr('method');

    var formData = {};

    $(this).find('[name]').each(function(index){
      formData[$(this).attr('name')] = $(this).val();
    });

    $.ajax({
      method: method,
      url:    action,
      data:   formData,
      success: function(data){
        alert('Thank you for subscribing to our newsletter.');
      },
      fail: function(xhr, status){
        alert('Subscribe option currently unavailable. Please try again later.');
      }
    });

  });
});