var lcm = require('jake-lcm')
//  , repl = require('repl')
  , mongoose = require('mongoose')

  , Advert = require('./models/advert.js')
  , News = require('./models/news.js')
  , User = require('./models/user.js')

  , Faker = require('Faker')
  , slug = require('./helpers/slugify.js');

//  , Q = require('q');

// get random array element
function randomElement(array) {
  return array[Math.floor(Math.random() * array.length)];
}

// make fake dates
function randomDate(start, end) {
  return new Date(
    start.getTime() + Math.random() * (end.getTime() - start.getTime())
  );
}

// make body
function randomBody() {
  // Faker.Lorem.paragraphs(10).replace(/\t/g, ''),
  var body = [];
  var paragraphs = Math.floor(Math.random() * 10) + 2;

  for(var p = 0; p < paragraphs; p++) {
    var paragraph = [];
    var sentences = Math.floor(Math.random() * 10) + 1;

    var blockQuote = Math.random();
    var emSentence = Math.random();

    var surround = '';
    if (emSentence < 0.1)
      surround = '*';

    for (var s = 0; s < sentences; s++) {
      paragraph.push(surround + Faker.Lorem.sentence() + '.' + surround);
    }

    var prefix = '';
    if (blockQuote < 0.2)
      prefix = '>';

    body.push(prefix + paragraph.join(' '));
  }

  return body.join('\n \r');
}

// make random click/impression data
function dateData(base, fluc) {
  var obj = {};

  var start = new Date(2013, 01, 01);
  var stop  = new Date();

  var current = start;
  while (current <= stop) {
    var day = parseInt(current.getDate(), 10);
    var month = parseInt(current.getMonth(), 10) + 1;

    if (month < 10) month = '0' + month
    if (day < 10) day = '0' + day

    var key = current.getFullYear() + '' + month + '' + day;

    // save!
    obj[key] = Math.floor(Math.random()*100);

    current.setDate(current.getDate() + 1);
    current = new Date(current);
  }

  return obj;
}

// start lcm environment
lcm.exec({
  initializers: ['00_generic', '02_mongoose', '03_models', '04_passport']
}, function () {

  // drop tables
  function dropTables(callback) {
    User.remove({}, function(err) {
      News.remove({}, function(err) {
        Advert.remove({}, function(err) {
          callback();
        });
      });
    });
  }

  // generate articles
  function generateArticles(count, callback) {
    var title = Faker.Lorem.sentence();

    var article = new News({
      title: title,
      title_slug: slug.toURL(title),

      content: randomBody(),

      category: randomElement(
        ['local', 'regional', 'worldwide', 'we-chat-with', 'sustainability',
         'oil-and-gas', 'infrastructure', 'rail-and-transport', 'ceramics']
      ),

      tags: Faker.Lorem.words(Faker.random.number(10)),
      magazines: randomElement(['oman', 'emirates', 'qatar', 'kuwait', 'saudi']),

      published: true,

      created_at: randomDate(new Date(1990, 1, 1), new Date()),
      published_at: randomDate(new Date(1990, 1, 1), new Date()),
      modified_at: randomDate(new Date(1990, 1, 1), new Date())

    });

    // save it
    article.save(function(err) {
      if (err)
        throw err;

      count--;
      if (count > 0)
        generateArticles(count, callback);
      else
        callback();
    });
  }

  // generate articles
  function generateAdverts(count, callback) {
    var ad = new Advert({
      link: 'http://www.google.co.uk/',

      // preloaded images
      images:  [
        'https://tpgmedia.s3.amazonaws.com/ads/5231756314c444802d000003/Desert.jpg',
        'https://tpgmedia.s3.amazonaws.com/ads/5231756314c444802d000003/Desert@2x.jpg'
      ],

      priority: 3000,
      magazines: randomElement(['oman', 'emirates', 'qatar', 'kuwait', 'saudi']),
      published: true,

      // clicks
      clicks_by_date: dateData(500, 20),
      clicks_last: new Date(),

      impressions_by_date: dateData(50, 10),
      impressions_last: new Date(),

      created_at: randomDate(new Date(1990, 1, 1), new Date()),
      published_at: randomDate(new Date(1990, 1, 1), new Date()),
      modified_at: randomDate(new Date(1990, 1, 1), new Date())
    });

    // save it
    ad.save(function(err) {
      if (err)
        throw err;

      count--;
      if (count > 0)
        generateAdverts(count, callback);
      else
        callback();
    });
  }

  // generate users
  function generateUsers(callback) {
    var admin = new User({
      name: 'Admin',
      email: 'admin@websir.co.uk',
      phone: '0123456789',
      password: 'testpass',

      subscriptions: [
        {
          magazine: 'oman',
          start_date: randomDate(new Date(1990, 1, 1), new Date()),
          end_date: new Date('2014-01-01')
        }
      ],

      authorisation: 4
    });

    admin.save(function(err) {
      callback();
    });
  }

  // let's go
  dropTables(function() {
    console.log('tables dropped');

    // haha...
    generateArticles(600, function() {
      console.log('news generated');

      // callback
      generateAdverts(20, function() {
        console.log('adverts generated');

        // hell
        generateUsers(function() {
          console.log('users generated');
          console.log(' `- admin@websir.co.uk, testpass');

          process.exit(0);
        });
      });
    });
  });
});
